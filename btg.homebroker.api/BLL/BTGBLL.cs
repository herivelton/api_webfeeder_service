﻿//using Microsoft.Extensions.Logging;
using btg.homebroker.api.Model;
using Newtonsoft.Json;
using RestSharp;
using Serilog;
using System;
using System.Net;
using System.Threading.Tasks;


namespace btg.homebroker.api.BLL
{
    public class BTGBLL
    {
        #region "Construtor"

        private RestClient _client;
        private RestRequest _request;
        public ILogger _logger;

        private string _endPointToken;
        private string _endPointWebFeeder = "";
        // private string _endPointWebFeeder02 = "";
        private string _issuer = "";
        private string _audience = "";
        private string _key = "";
        private string _user = "";
        private string _pass = "";
        private string _accountTest = "";

        public BTGBLL(string urlToken, 
                      string issuer,
                      string audience,
                      string key,
                      string servidorWebFeeder01,
                      // string servidorWebFeeder02,
                      string user,
                      string pass,
                      string accountTest)
        {
            _endPointToken = urlToken;

            _endPointWebFeeder = servidorWebFeeder01;
            // _endPointWebFeeder02 = servidorWebFeeder02;
            _user = user;
            _pass = pass;
            _accountTest = accountTest;

            _issuer = issuer;
            _audience = audience;
            _key = key;

            _client = new RestClient
            {
                BaseUrl = new Uri(_endPointToken),
            };
        }

        #endregion

        #region "PostLogin - FastTradeWeb"

        public async Task<TokenResponseModel> PostLogin_FastTradeWeb(string accountNumber, string ipAddress)
        {
            try
            {
                if (!string.IsNullOrEmpty(accountNumber))
                {
                    var tokenResponse = new TokenResponseModel(_endPointWebFeeder, _issuer, _audience, _key);
                    tokenResponse.isDebug = false;
                    tokenResponse.accountNumber = accountNumber;

                    return tokenResponse;
                }

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "PostLogin - BTG"

        public async Task<TokenResponseModel> PostLogin_BTG(string token, string ipAddress)
        {
            try
            {
                TokenResponseModel ret = validUserDemo_BTG(token);
                if (ret != null)
                {
                    LogMessage("STATUS=USER_DEMO [" + token + "] > " + ret.accountNumber, ipAddress);
                    return ret;
                }

                string urlService = "/api/sso/public/validate/token/";
                _request = new RestRequest(urlService, Method.POST) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessage("TOKEN=" + urlService, ipAddress);
                _request.AddBody(token);

                LogMessageRequest(_request, ipAddress);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request) as RestResponse;
                }).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    LogMessage(string.Format("STATUS={0} > {1}", (int)response.StatusCode, response.Content), ipAddress);
                    TryError(response, urlService);
                }
                else
                {
                    LogMessage("STATUS=200 > " + response.Content, ipAddress);
                }


                TokenResponseModel jsonResponse = new TokenResponseModel();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<TokenResponseModel>(response.Content);
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }

                if (!string.IsNullOrEmpty(jsonResponse.accountNumber))
                {
                    var tokenResponse = new TokenResponseModel(_endPointWebFeeder, _issuer, _audience, _key);
                    tokenResponse.isDebug = false;
                    tokenResponse.accountNumber = jsonResponse.accountNumber;
                    tokenResponse.token = jsonResponse.token;
                    LogMessage("TOKEN=" + jsonResponse.accountNumber, ipAddress);

                    return tokenResponse;
                }

                return jsonResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        /// <summary>
        /// Validar usuário DEMO
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private TokenResponseModel validUserDemo_BTG(string token) {
            if (string.IsNullOrEmpty(_accountTest))
            {
                return null;
            }
            if (token == "btg01" ||
                token == "btg02" ||
                token == "btg03" ||
                token == "dev01" ||
                token == "dev02" ||
                token == "dev03" ||
                token == "dev04" ||
                token == "dev05" ||
                token == "dev06" ||
                token == "dev07" ||
                token == "dev08" ||
                token == "dev09")
            {
                string accountNumberText = _accountTest;

                var tokenResponse = new TokenResponseModel(_endPointWebFeeder, _issuer, _audience, _key);
                tokenResponse.isDebug = true;
                tokenResponse.token = token;
                tokenResponse.accountNumber = accountNumberText;

                if (token == "btg01")
                {
                    tokenResponse.accountNumber = "265896";
                }
                else if (token == "btg02")
                {
                    tokenResponse.accountNumber = "228793";
                }
                else if (token == "btg03")
                {
                    tokenResponse.accountNumber = "98217";
                }

                return tokenResponse;
                // return "{ \"token\": \"" + token + "\",\"accountNumber\": \"" + accountNumberText + "\"}";
            }
            if (token == "dev00")
            {
                var tokenResponse = new TokenResponseModel();
                tokenResponse.code = "401";
                tokenResponse.codeMessage = "ERROR_LOGIN_0005";
                tokenResponse.message = "Unauthorized";
                return tokenResponse;
                // return "{ \"code\": \"401\", \"codeMessage\": \"ERROR_LOGIN_0005\", \"message\": \"Unauthorized\"}";
            }
            return null;
        }

        #endregion

        #region "PostLogin - VEXTER"

        public async Task<TokenResponseModel> validAccountNumber(string accountNumber, string ipAddress)
        {
            try
            {
                TokenResponseModel ret = validAccountDefault(accountNumber);
                if (ret != null)
                {
                    LogMessage("STATUS=USER_DEMO [" + accountNumber + "] > " + ret.accountNumber, ipAddress);
                    return ret;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Validar usuário DEFAULT
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        private TokenResponseModel validAccountDefault(string accountNumber)
        {
            if (string.IsNullOrEmpty(_accountTest))
            {
                return null;
            }

            string accountNumberText = _accountTest;

            var tokenResponse = new TokenResponseModel(_endPointWebFeeder, _issuer, _audience, _key);
            tokenResponse.isDebug = true;
            tokenResponse.token = accountNumber;
            tokenResponse.accountNumber = accountNumber;

            return tokenResponse;

        }

        #endregion

        #region "GetResponseContentAsync"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="theClient"></param>
        /// <param name="theRequest"></param>
        /// <returns></returns>
        public static Task<IRestResponse> GetResponseContentAsync(RestClient theClient, RestRequest theRequest)
        {
            var tcs = new TaskCompletionSource<IRestResponse>();
            theClient.ExecuteAsync(theRequest, response =>
            {
                tcs.SetResult(response);
            });
            return tcs.Task;
        }

        #endregion

        #region TryError

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <param name="urlService"></param>
        /// <returns></returns>
        private string TryError(IRestResponse response, string urlService)
        {
            var error = new
            {
                Method = response.Request.Method,
                HashCode = response.Request.GetHashCode(),
                Resource = response.Request.Resource,
                StatusCode = (int)response.StatusCode,
                Status = response.StatusCode,
                Content = response.Content
            };

            string messageError = string.Format("{0} {1} | StatusCode: {2} - {3}", response.Request.Method, response.Request.Resource, (int)response.StatusCode, response.StatusCode);

            Console.Error.WriteLine(messageError);
            Log.Error("Error: {@Error}", error);

            throw new Exception("StatusCode=" + Convert.ToString((int)response.StatusCode) + " - " + response.StatusDescription);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ipAddress"></param>
        private void LogMessageRequest(RestRequest request, string ipAddress)
        {
            string messageInfo = string.Format("[{2}] {0} {1}", request.Method, request.Resource, ipAddress);
            var info = new
            {
                IP = ipAddress,
                Method = request.Method,
                HashCode = request.GetHashCode(),
                Resource = request.Resource
            };

            Console.WriteLine(messageInfo);
            Log.Warning("Request: {@Info}", info, request.Parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageInfo"></param>
        /// <param name="ipAddress"></param>
        private void LogMessage(string messageInfo, string ipAddress)
        {
            messageInfo = string.Format("[{0}] {1}", ipAddress, messageInfo);
            Console.WriteLine(messageInfo);
            Log.Warning(messageInfo);
        }

        #endregion
    }
}
