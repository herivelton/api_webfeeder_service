﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using btg.homebroker.api.Model;
using Newtonsoft.Json;
// using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Serilog;

namespace btg.homebroker.api.BLL
{
    public class WebfeederServicesBLL
    {
        private const string MODULO_QUOTE = "services/quotes/";
        private const string MODULO_NEGOTIATION = "services/negotiation/";
        private const string MODULO_NEWS = "services/news/";
        private const string MODULO_CHART = "services/chart/";

        private const int TOTAL_MINUTES_FOR_REFRESH_TOKEN = 25;
        private const string HEADER_USER_IDENTIFIER = "user-identifier";

        public DateTime? dataLoginWebFeeder = null;

        #region Metodo - Login

        private RestClient _client;
        private RestRequest _request;
        public ILogger _logger;
        // private readonly IHttpContextAccessor _contextAccessor;

        private string _userWebFeeder;
        private string _passWebFeeder;
        private string _endPoint;
        private string _source;

        LoginResponseModel loginWebfeederResponse = new LoginResponseModel();
        QuoteResponseModel quoteDLResponse = new QuoteResponseModel();

        /// <summary>
        /// Realizar login no Webfeeder
        /// </summary>
        /// <param name="source"></param>
        /// <param name="user"></param>
        /// <param name="senha"></param>
        /// <param name="servidor"></param>
        public WebfeederServicesBLL(string source, string user, string senha, string servidor) // , string servidor02)
        {
            _userWebFeeder = user;
            _passWebFeeder = senha;
            _source = source;
            _endPoint = servidor;

            _client = new RestClient
            {
                BaseUrl = new Uri(_endPoint),
                Authenticator = new SimpleAuthenticator("login", _userWebFeeder, "password", _passWebFeeder),
                CookieContainer = new CookieContainer(),
            };

            loginWebfeederSync("");
        }

        private LoginResponseModel loginWebfeederSync(string ipAddress)
        {
            try
            {
                //Request de autenticação
                _request = new RestRequest("SignIn", Method.POST);
                LogMessage("Login: WebFeeder [" + this._endPoint + "]", ipAddress);

                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, null) as RestResponse;
                }).Wait();


                //Validação da resposta
                if (response.Content == "true")
                {
                    LogMessage("Login WebFeeder[" + this._endPoint + "]: Conectado", ipAddress);
                    loginWebfeederResponse.Msg = "Conectado.";
                    loginWebfeederResponse.Status = response.Content;
                    this.dataLoginWebFeeder = DateTime.Now;

                    return loginWebfeederResponse;
                }
                else
                {
                    LogMessage("Login WebFeeder[" + this._endPoint + "]: Erro na Autenticação - " + response.Content, ipAddress);
                    loginWebfeederResponse.Msg = "Erro na Autenticação.";
                    loginWebfeederResponse.Status = response.Content;
                    this.dataLoginWebFeeder = null;
                }
                return loginWebfeederResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public LoginResponseModel RefreshTokenSync(string ipAddress)
        {
            LogMessage("Login: RefreshTokenSync", ipAddress);
            _client.CookieContainer = new CookieContainer();
            return loginWebfeederSync(ipAddress);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="theClient"></param>
        /// <param name="theRequest"></param>
        /// <param name="tokenJWT"></param>
        /// <returns></returns>
        public static Task<IRestResponse> GetResponseContentAsync(RestClient theClient, RestRequest theRequest, MessageTokenJWT tokenJWT)
        {
            if (tokenJWT != null)
            {
                theRequest.AddHeader(HEADER_USER_IDENTIFIER, tokenJWT.headerUserIdentifier);
            }
            theRequest.Timeout = 30;
            var tcs = new TaskCompletionSource<IRestResponse>();
            theClient.ExecuteAsync(theRequest, response =>
            {
                tcs.SetResult(response);
            });
            return tcs.Task;
        }
        // TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
        // RestRequestAsyncHandle handle = theClient.ExecuteAsync(theRequest, r => taskCompletion.SetResult(r));
        // return (RestResponse) (await taskCompletion.Task);

        //public static class RestClientExtensions
        //{
        //    public static async Task<RestResponse> ExecuteAsync(this RestClient client, RestRequest request)
        //    {
        //        TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
        //        RestRequestAsyncHandle handle = client.ExecuteAsync(request, r => taskCompletion.SetResult(r));
        //        return (RestResponse)(await taskCompletion.Task);
        //    }
        //}

        public bool GetTokenRefresh()
        {
            if (this.dataLoginWebFeeder.HasValue &&
                DateTime.Now.Subtract(this.dataLoginWebFeeder.Value).TotalMinutes >= TOTAL_MINUTES_FOR_REFRESH_TOKEN)
            {
                return true;
            }
            return false;
        }
        public void SetTokenRefresh()
        {
            this.dataLoginWebFeeder = DateTime.Now;
        }

        #endregion

        #region Metodo - RefreshTokenAsync

        public async Task<LoginResponseModel> RefreshTokenAsync(string ipAddress)
        {
            LogMessage("Login: RefreshTokenAsync", ipAddress);
            _client.CookieContainer = new CookieContainer();
            return await loginWebfeederAsync(ipAddress);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        private async Task<LoginResponseModel> loginWebfeederAsync(string ipAddress)
        {

            try
            {
                //Request de autenticação
                _request = new RestRequest("SignIn", Method.POST);
                LogMessage("Login: WebFeeder [" + this._endPoint + "]", ipAddress);

                //IRestResponse response = new RestResponse();
                //Task.Run(async () =>
                //{
                //    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                //}).Wait();
                IRestResponse response = await GetResponseContentAsync(_client, _request, null);


                //Validação da resposta
                if (response.Content == "true")
                {
                    LogMessage("Login WebFeeder[" + this._endPoint + "]: Conectado", ipAddress);
                    loginWebfeederResponse.Msg = "Conectado.";
                    loginWebfeederResponse.Status = response.Content;

                    return loginWebfeederResponse;
                }
                else
                {
                    LogMessage("Login WebFeeder[" + this._endPoint + "]: Erro na Autenticação - " + response.Content, ipAddress);
                    loginWebfeederResponse.Msg = "Erro na Autenticação.";
                    loginWebfeederResponse.Status = response.Content;
                }
                return loginWebfeederResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region Metodo - services/quotes

        #region Metodo - ListQuoteMarket and ListCompanyMarket

        public string GetListQuoteMarketString(string market, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = MODULO_QUOTE + "listQuoteMarket/" + market.ToLower();
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetListCompanyMarketString(string market, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/listCompanyMarket/";
                _request = new RestRequest(urlService + market.ToLower(), Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region Metodo - ListMarket

        public string GetListMarketString(string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/listMarket/";
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Metodo - GetQuote

        public async Task<string> GetQuoteQuery(string query, int? qtdRegister, string getFracional, string getOptions, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                if (!qtdRegister.HasValue || qtdRegister == 0)
                {
                    qtdRegister = 10;
                }
                if (string.IsNullOrEmpty(getFracional))
                {
                    getFracional = "true";
                }
                if (string.IsNullOrEmpty(getOptions))
                {
                    getOptions = "true";
                }
                string urlService = string.Format("services/quotes/quotesQuery?query={0}&n={1}&f={2}&o={3}", query.ToLower(), qtdRegister, getFracional, getOptions);
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Metodo - GetQuote

        public async Task<string> GetQuoteString(string ativo, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = MODULO_QUOTE + "quote/";
                _request = new RestRequest(urlService + ativo.ToLower(), Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public async Task<string> GetCandleDate(string symbol, string resolution, string from, string to)
        //{
        //    try
        //    {
        //        var dateFrom = UnixTimeStampToDateTime(Convert.ToDouble(from));
        //        var dateTo = UnixTimeStampToDateTime(Convert.ToDouble(to));

        //        string dataStart = dateFrom.ToString("yyyyMMddHHmm");
        //        string dataEnd = dateTo.ToString("yyyyMMddHHmm");

        //        // services/chart/candleChart/petr4/5/fdddd/10/true/201707271453/201707271453
        //        // http://localhost:8080/services/quotes/candleDate/petr4/15/201201010000/201201050000

        //        string urlService = string.Format(MODULO_QUOTE + "candleDate/{0}/{1}/{2}/{3}",
        //                                          symbol,
        //                                          resolution,
        //                                          dataStart,
        //                                          dataEnd);
        //        _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

        //        LogMessageRequest(_request);
        //        IRestResponse response = await GetResponseContentAsync(_client, _request);

        //        if (response.StatusCode != HttpStatusCode.OK)
        //        {
        //            TryError(response, urlService);
        //        }
        //        return response.Content;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="symbol">Qual ativo deve ser retornado as informações;</param>
        /// <param name="resolution">Qual período a ser buscado no serviço 
        /// (S-semanal, D-diário, M-mensal, minutos passar valores(1, 5, 10, 15, 30, 60));</param>
        /// <param name="token"></param>
        /// <param name="from">Data inicial para que seja retornado os candles apenas a partir desta data e hora, formato YYYYMMDDHHMM.</param>
        /// <param name="to">Data final para que seja retornado os candles apenas até esta data e hora, formato YYYYMMDDHHMM.</param>
        /// <returns></returns>
        public async Task<string> GetCandleChart(string symbol, string resolution, string token, string from, string to, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                var maxCandle = 1000;
                var realTime = true;
                var dateFrom = Convert.ToDateTime(from);
                var dateTo = Convert.ToDateTime(to);

                string dataStart = dateFrom.ToString("yyyyMMddHHmm");
                string dataEnd = dateTo.ToString("yyyyMMddHHmm");

                //if (dateTo.Subtract(dateFrom).Days > 30) {
                //    dataStart = dateTo.AddMonths(-1).ToString("yyyyMMddHHmm");
                //}
                // services/chart/candleChart/petr4/5/fdddd/10/true/201707271453/201707271453

                string urlService = string.Format(MODULO_CHART + "candleChart/{0}/{1}/{2}/{3}/{4}/{5}/{6}",
                                                  symbol,
                                                  resolution,
                                                  token,
                                                  maxCandle,
                                                  realTime,
                                                  dataStart,
                                                  dataEnd);
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //private DateTime UnixTimeStampToDateTime(Int64 unixTimeStamp)
        //{
        //    //// Unix timestamp is seconds past epoch
        //    //System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        //    //dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        //    //return dtDateTime;
        //    var maxValue = TimeSpan.MaxValue;

        //    var timeSpan = TimeSpan.Parse(Convert.ToString(unixTimeStamp));
        //    var unixTime = new DateTime(timeSpan.Ticks).ToLocalTime();
        //    return unixTime;
        //}

        public async Task<string> GetQuoteInformation(string query, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/quoteInformation?description=";
                _request = new RestRequest(urlService + query.ToLower(), Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Retorna informações dos ativos 
        /// </summary>
        /// <param name="ativo"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public async Task<QuoteResponseModel> GetQuote(string ativo, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/quote/";
                _request = new RestRequest(urlService + ativo.ToLower(), Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return JsonConvert.DeserializeObject<QuoteResponseModel>(response.Content);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Metodo - GetListQuote

        public async Task<List<ListQuoteResponseModel>> GetListQuote(string market, string typeQuote, string description, string speak, string expiration, string company, string contract, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                List<ListQuoteResponseModel> retornoList = new List<ListQuoteResponseModel>();

                // BOVESPA e BMF
                if (market == "-1")
                {
                    //string retornoBovespa = "";
                    //string retornoBMF = "";
                    var retornoBovespa = await GetListQuoteWebFeeder("1", typeQuote, description, speak, expiration, company, contract, ipAddress, tokenJWT);
                    var retornoBMF = await GetListQuoteWebFeeder("3", typeQuote, description, speak, expiration, company, contract, ipAddress, tokenJWT);

                    //retornoBovespa = retornoBovespa.Replace("[", "").Replace("]", "").Replace("\n", "");
                    //retornoBMF = retornoBMF.Replace("[", "").Replace("]", "").Replace("\n", "");

                    //string sVirgula = "";
                    //if (!string.IsNullOrEmpty(retornoBovespa) && !string.IsNullOrEmpty(retornoBMF)) {
                    //    sVirgula = ",";
                    //}
                    //retornoList = "[" + retornoBovespa + sVirgula + retornoBMF + "]";
                    retornoList.AddRange(retornoBovespa);
                    retornoList.AddRange(retornoBMF);
                }
                // BOVESPA
                else if (market == "1")
                {
                    retornoList = await GetListQuoteWebFeeder("1", typeQuote, description, speak, expiration, company, contract, ipAddress, tokenJWT);
                }
                // BMF
                else if (market == "3")
                {
                    retornoList = await GetListQuoteWebFeeder("3", typeQuote, description, speak, expiration, company, contract, ipAddress, tokenJWT);
                }
                // QUOTE para planilha de cotação
                else if (market == "2")
                {
                    if (!string.IsNullOrEmpty(speak) && speak != "-")
                    {
                        speak = speak.ToLower();
                        var array = speak.Split('*');
                        if (array.Length == 2)
                        {
                            var symbol = array[0];
                            int size = 0;
                            if (int.TryParse(array[1], out size))
                            {

                                if (symbol.Length == 3)
                                {
                                    // Consultar contrato futuro de BMF
                                    market = "3";
                                    typeQuote = "7"; // Futuro
                                }
                                else if (symbol.Length == 4)
                                {
                                    // Consultar opções de BOVESPA
                                    market = "1";
                                    typeQuote = "2"; // Opcoes
                                }
                                retornoList = await GetListQuoteWebFeeder(market, typeQuote, description, symbol, expiration, company, contract, ipAddress, tokenJWT);

                                // Ordenar por vencimento
                                retornoList = (from item in retornoList
                                               where item.expiration.HasValue
                                               orderby item.expiration ascending
                                               select item).ToList();

                                if (size > 10)
                                {
                                    size = 10;
                                }
                                // Filtrar por tamanho do retorno
                                if (retornoList.Count > size)
                                {
                                    retornoList = retornoList.Take(size).ToList();
                                }
                            }
                        }
                        else
                        {
                            //market = "-1"
                        }
                    }
                }
                return retornoList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<List<ListQuoteResponseModel>> GetListQuoteWebFeeder(string market, string typeQuote, string description, string speak, string expiration, string company, string contract, string ipAddress, MessageTokenJWT tokenJWT)
        {
            string auxSearch = string.Empty;

            try
            {

                if (market == "1" && company != "-")
                {
                    auxSearch = description;
                    description = company;
                }
                else if (market == "3" && contract != "0")
                {
                    auxSearch = speak;
                    speak = contract;
                }

                string urlService = "services/quotes/listQuote/";
                _request = new RestRequest(urlService + market.ToLower() + "/" + typeQuote.ToLower() + "/" + description + "/" + speak + "/" + expiration, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                List<ListQuoteResponseModel> jsonResponse = new List<ListQuoteResponseModel>();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<List<ListQuoteResponseModel>>(response.Content);

                    if (jsonResponse.Count > 0)
                    {
                        if (market == "1")
                        {
                            description = description.ToUpper();

                            if (!string.IsNullOrEmpty(auxSearch) && auxSearch != "-" && !string.IsNullOrEmpty(speak) && speak != "-")
                            {

                                jsonResponse = (from item in jsonResponse
                                                where (item.empresa != null
                                                   && item.empresa.ToUpper().Contains(auxSearch.ToUpper())) && (item.description.ToUpper().StartsWith(speak.ToUpper()))
                                                select item).ToList();

                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(speak) && speak != "-")
                                {
                                    jsonResponse = (from item in jsonResponse
                                                    where item.description != null
                                                   && item.description.ToUpper().Contains(speak.ToUpper())
                                                    select item).ToList();
                                }

                                if (!string.IsNullOrEmpty(auxSearch) && auxSearch != "-")
                                {
                                    jsonResponse = (from item in jsonResponse
                                                    where item.empresa != null
                                                   && item.empresa.ToUpper().Contains(auxSearch.ToUpper())
                                                    select item).ToList();
                                }
                            }
                        }
                        else if (market == "3")
                        {
                            if (!string.IsNullOrEmpty(auxSearch) && auxSearch != "-" && !string.IsNullOrEmpty(description) && description != "-")
                            {
                                jsonResponse = (from item in jsonResponse
                                                where (item.description != null
                                                   && item.description.ToUpper().StartsWith(auxSearch.ToUpper())) &&
                                                   (item.empresa != null
                                                   && item.empresa.ToUpper().StartsWith(description.ToUpper()))
                                                select item).ToList();
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(auxSearch) && auxSearch != "-")
                                {
                                    jsonResponse = (from item in jsonResponse
                                                    where item.description != null
                                                       && item.description.ToUpper().StartsWith(auxSearch.ToUpper())
                                                    select item).ToList();
                                }

                                if (!string.IsNullOrEmpty(description) && description != "-")
                                {
                                    jsonResponse = (from item in jsonResponse
                                                    where item.empresa != null
                                                       && item.empresa.ToUpper().StartsWith(description.ToUpper())
                                                    select item).ToList();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }
                return jsonResponse;
                //return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Metodo - GetHighList

        public async Task<List<MarketHighlightsResponseModel>> GetHighList(string indice, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/highList/";
                _request = new RestRequest(urlService + indice.ToLower(), Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }


                List<MarketHighlightsResponseModel> jsonResponse = new List<MarketHighlightsResponseModel>();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<List<MarketHighlightsResponseModel>>(response.Content);
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }

                return jsonResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region Metodo - GetFallList

        public async Task<List<MarketHighlightsResponseModel>> GetFallList(string indice, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/fallList/";
                _request = new RestRequest(urlService + indice.ToLower(), Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }


                List<MarketHighlightsResponseModel> jsonResponse = new List<MarketHighlightsResponseModel>();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<List<MarketHighlightsResponseModel>>(response.Content);
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }
                return jsonResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region Metodo - GetMarketSectors

        public async Task<string> GetMarketSectors(string indice, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/marketSectors?market=";
                _request = new RestRequest(urlService + indice.ToLower(), Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region Metodo - GetMarketSubSectorQuotes

        public async Task<string> GetMarketSubSectorQuotes(string indice, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/marketSubSectorQuotes?sub=";
                _request = new RestRequest(urlService + indice, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region Metodo - GetFallVolumeList

        public async Task<List<MarketHighlightsResponseModel>> GetFallVolumeList(string indice, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/fallVolumeList/";
                _request = new RestRequest(urlService + indice.ToLower(), Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                List<MarketHighlightsResponseModel> jsonResponse = new List<MarketHighlightsResponseModel>();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<List<MarketHighlightsResponseModel>>(response.Content);
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }

                return jsonResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        #endregion

        #region Metodo - GetHighVolumeList

        public async Task<List<MarketHighlightsResponseModel>> GetHighVolumeList(string indice, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/highVolumeList/";
                _request = new RestRequest(urlService + indice.ToLower(), Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                List<MarketHighlightsResponseModel> jsonResponse = new List<MarketHighlightsResponseModel>();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<List<MarketHighlightsResponseModel>>(response.Content);
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }

                return jsonResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        #endregion

        #region Metodo - GetListTypeQuotes

        public async Task<string> GetListTypeQuotes(string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/listTypeQuotes";
                //string filtro = "";
                //if (!(string.IsNullOrEmpty(market)))
                //{
                //    filtro = "?market=" + market;
                //}
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Metodo - GetPlayerNames

        public async Task<string> GetPlayerNames(string market, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/playerNames";
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                if (!(string.IsNullOrEmpty(market)))
                {
                    _request.AddQueryParameter("market", market);
                }

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return response.Content;
                }
                return TryError(response, urlService, tokenJWT);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Metodo - GetListContractBMF

        public async Task<string> GetListContractBMF(string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/listContractBMF";
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return response.Content;
                }
                return TryError(response, urlService, tokenJWT);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Metodo - GetListCompanyMarket

        public async Task<List<CompanyResponseModel>> GetListCompanyMarket(string marketCode, string nameCompany, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/quotes/listCompanyMarket/" + marketCode;
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                List<CompanyResponseModel> jsonResponse = new List<CompanyResponseModel>();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<List<CompanyResponseModel>>(response.Content);

                    if (jsonResponse.Count > 0)
                    {
                        jsonResponse = (from item in jsonResponse
                                        where item.name != null
                                        select item).ToList();

                        if (!string.IsNullOrEmpty(nameCompany))
                        {
                            nameCompany = nameCompany.ToUpper();

                            jsonResponse = (from item in jsonResponse
                                            where item.name.Contains(nameCompany)
                                            select item).ToList();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }
                return jsonResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetHighListNegotiation

        /// <summary>
        /// Lista dos mais negociadas a vista
        /// </summary>
        /// <param name="market"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public async Task<string> GetHighListNegotiation(string market, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = MODULO_QUOTE + "highListNegotiation/" + market.ToLower();
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #endregion


        #region Metodo - services/negotiation

        #region SendNewOrderSingle

        public async Task<string> SendNewOrderSingle(
            string account, string market, string price, string quote, int qtd, string validityorder,
            string targettrigger, string targetlimit, string stoptrigger, string stoplimit, string type,
            string side, string username, string orderstrategy, int loteDefault, string ipAddress, string timeinforce, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = MODULO_NEGOTIATION + "sendNewOrderSingle/";
                _request = new RestRequest(urlService, Method.POST) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddQueryParameter("account", account);
                _request.AddQueryParameter("market", market);

                if (!(type.Equals("START")) && !(type.Equals("STOP")))
                {
                    _request.AddQueryParameter("price", price.ToString());
                }

                _request.AddQueryParameter("quote", quote);
                _request.AddQueryParameter("qtd", qtd.ToString());

                if (timeinforce != null)
                    _request.AddQueryParameter("timeinforce", timeinforce.ToString());

                if (!string.IsNullOrEmpty(validityorder) && Convert.ToDateTime(validityorder) >= DateTime.Now.Date)
                    _request.AddQueryParameter("validityorder", Convert.ToDateTime(validityorder).ToString("ddMMyyyy"));

                if (targettrigger != null)
                    _request.AddQueryParameter("targettrigger", targettrigger.ToString());

                if (targetlimit != null)
                    _request.AddQueryParameter("targetlimit", targetlimit.ToString());

                if (stoptrigger != null)
                    _request.AddQueryParameter("stoptrigger", stoptrigger.ToString());

                if (stoplimit != null)
                    _request.AddQueryParameter("stoplimit", stoplimit.ToString());

                _request.AddQueryParameter("type", type);
                _request.AddQueryParameter("side", side);
                _request.AddQueryParameter("username", username);
                _request.AddQueryParameter("sourceaddress", ipAddress + "#" + _source);

                if (orderstrategy != null)
                    _request.AddQueryParameter("orderstrategy", orderstrategy.ToString());

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);
                //IRestResponse response = new RestResponse();
                //Task.Run(async () =>
                //{
                //    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                //}).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetHistoryOrder

        /// <summary>
        /// 
        /// </summary>
        /// <param name="account"></param>
        /// <param name="market"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="symbol"></param>
        /// <param name="status"></param>
        /// <param name="direction"></param>
        /// <param name="orderID"></param>
        /// <param name="type"></param>
        /// <param name="statusGroup"></param>
        /// <param name="queryType"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public async Task<string> GetHistoryOrder(string account,
                                                  string market,
                                                  string dataInicio,
                                                  string dataFim,
                                                  string symbol,
                                                  string status,
                                                  string direction,
                                                  string orderID,
                                                  string type,
                                                  string statusGroup,
                                                  string queryType,
                                                  string ipAddress,
                                                  MessageTokenJWT tokenJWT)
        {
            try
            {
                var dataInicioTemp = dataInicio;
                var dataFimTemp = dataFim;

                if (!string.IsNullOrEmpty(dataInicio) && !string.IsNullOrEmpty(dataFim))
                {
                    dataInicio = DateTime.Parse(dataInicio).ToString("ddMMyyyy");
                    dataFim = DateTime.Parse(dataFim).AddDays(1).ToString("ddMMyyyy");
                }
                else if (!string.IsNullOrEmpty(dataInicio))
                {
                    dataInicio = DateTime.Parse(dataInicio).ToString("ddMMyyyy");
                    dataFim = DateTime.Parse(dataInicioTemp).AddDays(1).ToString("ddMMyyyy");
                }
                else if (string.IsNullOrEmpty(orderID))
                {
                    dataInicio = DateTime.Now.ToString("ddMMyyyy");
                    dataFim = DateTime.Now.AddDays(1).ToString("ddMMyyyy");
                }
                else
                {
                    dataInicio = "-";
                    dataFim = "-";
                }

                // URL > /services/negotiation/historyOrder/account/market/dataInicio/dataFim/symbol/status/direction/orderId/statusGeneric/queryType
                string urlService = MODULO_NEGOTIATION + "historyOrder/{account}/{market}/{dataInicio}/{dataFim}/{symbol}/{status}/{direction}/{orderID}/{statusGroup}/{queryType}";

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddUrlSegment("account", account);
                _request.AddUrlSegment("market", market);
                _request.AddUrlSegment("dataInicio", dataInicio);

                if (string.IsNullOrEmpty(dataFim) && !string.IsNullOrEmpty(dataInicio))
                {
                    _request.AddUrlSegment("dataFim", dataFim);
                }
                else if (!string.IsNullOrEmpty(dataFim))
                {
                    // _request.AddUrlSegment("dataFim", dataFim);
                    _request.AddUrlSegment("dataFim", dataFim);
                }
                else
                {
                    _request.AddUrlSegment("dataFim", '-');
                }

                if (!string.IsNullOrEmpty(symbol))
                {
                    _request.AddUrlSegment("symbol", symbol);
                }
                else
                {
                    _request.AddUrlSegment("symbol", '-');
                }

                if (!string.IsNullOrEmpty(status))
                {
                    _request.AddUrlSegment("status", status);
                }
                else
                {
                    _request.AddUrlSegment("status", '-');
                }

                if (!string.IsNullOrEmpty(direction))
                {
                    _request.AddUrlSegment("direction", direction);
                }
                else
                {
                    _request.AddUrlSegment("direction", '-');
                }

                if (!string.IsNullOrEmpty(orderID))
                {
                    _request.AddUrlSegment("orderID", orderID);
                }
                else
                {
                    _request.AddUrlSegment("orderID", '-');
                }

                if (!string.IsNullOrEmpty(statusGroup))
                {
                    _request.AddUrlSegment("statusGroup", statusGroup);
                }
                else
                {
                    _request.AddUrlSegment("statusGroup", '-');
                }

                if (!string.IsNullOrEmpty(queryType))
                {
                    _request.AddUrlSegment("queryType", queryType);
                }
                else
                {
                    _request.AddUrlSegment("queryType", '-');
                }

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);


                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region CancelOrder

        public async Task<string> CancelOrder(string username,
                                              string orderid,
                                              string origclordid,
                                              string market,
                                              string quote,
                                              string qtd,
                                              string side,
                                              string enteringtrader,
                                              string account,
                                              string ipAddress,
                                              MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = MODULO_NEGOTIATION + "cancelOrder/";
                _request = new RestRequest(urlService, Method.POST) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                if (!string.IsNullOrEmpty(orderid))
                {
                    _request.AddQueryParameter("orderid", orderid);
                }

                if (!string.IsNullOrEmpty(origclordid))
                {
                    _request.AddQueryParameter("origclordid", origclordid);
                }

                if (!string.IsNullOrEmpty(quote))
                {
                    _request.AddQueryParameter("quote", quote);
                }

                if (!string.IsNullOrEmpty(side))
                {
                    _request.AddQueryParameter("side", side);
                }

                if (!string.IsNullOrEmpty(qtd))
                {
                    _request.AddQueryParameter("qtd", qtd.Replace(".", ""));
                }

                if (!string.IsNullOrEmpty(username))
                {
                    _request.AddQueryParameter("username", username);
                }

                if (!string.IsNullOrEmpty(market))
                {
                    _request.AddQueryParameter("market", market);
                }

                _request.AddQueryParameter("sourceaddress", ipAddress + "#" + _source);

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                //IRestResponse response = new RestResponse();
                //Task.Run(async () =>
                //{
                //    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                //}).Wait();

                //if (response.ErrorException != null)
                //{
                //    throw new Exception("Erro ao enviar a ordem.", response.ErrorException);
                //}

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region SendEditOrder

        public async Task<string> SendEditOrder(
            string account, string origclordid, string orderid, string market, string price, string quote, int qtd, string maxfloor, string validityorder,
            string targettrigger, string targetlimit, string stoptrigger, string stoplimit, string movingstart, string initialchangetype, string type,
            string initialchange, string pricepercentage, string side, string username, string enteringtrader, string ipAddress, string timeinforce, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = MODULO_NEGOTIATION + "editOrder/";
                _request = new RestRequest(urlService, Method.POST) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddQueryParameter("account", account);

                if (!string.IsNullOrEmpty(origclordid))
                    _request.AddQueryParameter("origclordid", origclordid);

                if (!string.IsNullOrEmpty(orderid))
                    _request.AddQueryParameter("orderid", orderid);

                _request.AddQueryParameter("market", market);

                if (!(type.Equals("START")) && !(type.Equals("STOP")))
                {
                    _request.AddQueryParameter("price", price.ToString());
                }

                _request.AddQueryParameter("quote", quote);
                _request.AddQueryParameter("qtd", qtd.ToString());

                if (!string.IsNullOrEmpty(maxfloor))
                    _request.AddQueryParameter("maxfloor", maxfloor);

                //DateTime enteredDate = DateTime.Parse(validityorder);

                if (timeinforce != null)
                    _request.AddQueryParameter("timeinforce", timeinforce.ToString());

                if (validityorder != null && Convert.ToDateTime(validityorder) >= DateTime.Now.Date)
                    _request.AddQueryParameter("validityorder", Convert.ToDateTime(validityorder).ToString("ddMMyyyy"));

                if (targettrigger != null)
                    _request.AddQueryParameter("targettrigger", targettrigger.ToString());

                if (targetlimit != null)
                    _request.AddQueryParameter("targetlimit", targetlimit.ToString());

                if (stoptrigger != null)
                    _request.AddQueryParameter("stoptrigger", stoptrigger.ToString());

                if (stoplimit != null)
                    _request.AddQueryParameter("stoplimit", stoplimit.ToString());

                if (!string.IsNullOrEmpty(movingstart))
                    _request.AddQueryParameter("movingstart", movingstart);

                if (!string.IsNullOrEmpty(initialchangetype))
                    _request.AddQueryParameter("initialchangetype", initialchangetype);

                if (!string.IsNullOrEmpty(initialchange))
                    _request.AddQueryParameter("initialchange", initialchange);

                if (!string.IsNullOrEmpty(pricepercentage))
                    _request.AddQueryParameter("pricepercentage", pricepercentage);

                _request.AddQueryParameter("type", type);
                _request.AddQueryParameter("side", side);
                _request.AddQueryParameter("username", username);

                if (!string.IsNullOrEmpty(enteringtrader))
                    _request.AddQueryParameter("enteringtrader", enteringtrader);

                _request.AddQueryParameter("sourceaddress", ipAddress + "#" + _source);

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);

                //IRestResponse response = new RestResponse();
                //Task.Run(async () =>
                //{
                //    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                //}).Wait();

                //if (response.ErrorException != null)
                //{
                //    throw new Exception("Erro ao enviar a ordem.", response.ErrorException);
                //}

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetPositionBovespa

        public async Task<string> GetPositionBovespa(string account, string quote, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = MODULO_NEGOTIATION + "positionBovespa/{account}/{quote}";

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddUrlSegment("account", account);

                if (!string.IsNullOrEmpty(quote))
                {
                    _request.AddUrlSegment("quote", quote);
                }
                else
                {
                    _request.AddUrlSegment("quote", string.Empty);
                }

                LogMessageRequest(_request, tokenJWT);
                //IRestResponse response = await GetResponseContentAsync(_client, _request);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetPositionBmf

        public async Task<string> GetPositionBmf(string account, string quote, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = MODULO_NEGOTIATION + "positionBmf/{account}/{quote}";

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddUrlSegment("account", account);

                if (!string.IsNullOrEmpty(quote))
                {
                    _request.AddUrlSegment("quote", quote);
                }
                else
                {
                    _request.AddUrlSegment("quote", string.Empty);
                }

                LogMessageRequest(_request, tokenJWT);
                //IRestResponse response = await GetResponseContentAsync(_client, _request);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetDayPosition

        public async Task<PositionResponseModel> GetDayPosition(string account, string quote, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {

                string urlService = MODULO_NEGOTIATION + "positionBovespa/{account}/{quote}";

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddUrlSegment("account", account);

                if (!string.IsNullOrEmpty(quote))
                {
                    _request.AddUrlSegment("quote", quote);
                }
                else
                {
                    _request.AddUrlSegment("quote", string.Empty);
                }

                LogMessageRequest(_request, tokenJWT);
                //IRestResponse responseBovespa = await GetResponseContentAsync(_client, _request);
                IRestResponse responseBovespa = new RestResponse();
                Task.Run(async () =>
                {
                    responseBovespa = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (responseBovespa.StatusCode != HttpStatusCode.OK)
                {
                    TryError(responseBovespa, urlService, tokenJWT);
                }
                PositionResponseModel listResult = JsonConvert.DeserializeObject<PositionResponseModel>(responseBovespa.Content);



                urlService = MODULO_NEGOTIATION + "positionBmf/{account}/{quote}";

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddUrlSegment("account", account);

                if (!string.IsNullOrEmpty(quote))
                {
                    _request.AddUrlSegment("quote", quote);
                }
                else
                {
                    _request.AddUrlSegment("quote", string.Empty);
                }

                LogMessageRequest(_request, tokenJWT);
                //IRestResponse responseBmf = await GetResponseContentAsync(_client, _request);
                IRestResponse responseBmf = new RestResponse();
                Task.Run(async () =>
                {
                    responseBmf = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (responseBmf.StatusCode != HttpStatusCode.OK)
                {
                    TryError(responseBmf, urlService, tokenJWT);
                }
                listResult.custodyBmf = JsonConvert.DeserializeObject<PositionResponseModel>(responseBmf.Content).custodyBmf;
                listResult.currentPosition = listResult.MergeListsBmfBovespa();
                return listResult;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetFinancialAccountInformation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="account"></param>
        /// <param name="market"></param>
        /// <returns></returns>
        public async Task<string> GetFinancialAccountInformation(string account, string market, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = MODULO_NEGOTIATION + "financialAccountInformation/{account}/{market}";

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddUrlSegment("account", account);
                _request.AddUrlSegment("market", market);

                LogMessageRequest(_request, tokenJWT);
                //IRestResponse response = await GetResponseContentAsync(_client, _request);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetFinancialAccountInformationCompl
        /// <summary>
        /// 
        /// </summary>
        /// <param name="account"></param>
        /// <param name="market"></param>
        /// <returns></returns>
        public async Task<string> GetFinancialAccountInformationCompl(string account, string market, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = MODULO_NEGOTIATION + "financialAccountInformationCompl/{account}/{market}";

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddUrlSegment("account", account);
                _request.AddUrlSegment("market", market);

                LogMessageRequest(_request, tokenJWT);
                //IRestResponse response = await GetResponseContentAsync(_client, _request);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetLeverageQuote
        /// <summary>
        /// 
        /// </summary>
        /// <param name="account"></param>
        /// <param name="market"></param>
        /// <param name="quote"></param>
        /// <returns></returns>
        public async Task<string> GetLeverageQuote(string account, string market, string quote, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/negotiation/leverageQuote/{account}/{market}/{quote}";

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddUrlSegment("account", account);
                _request.AddUrlSegment("market", market.ToUpper());
                _request.AddUrlSegment("quote", quote.ToLower());

                LogMessageRequest(_request, tokenJWT);
                //IRestResponse response = await GetResponseContentAsync(_client, _request);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetStatementAccount
        /// <summary>
        /// 
        /// </summary>
        /// <param name="account"></param>
        /// <param name="market"></param>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public async Task<string> GetStatementAccount(string account, string market, string dateStart, string dateEnd, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/negotiation/statementBovespa/{account}/FINAN/{dateStart}/{dateEnd}";
                if (market == "XBMF")
                {
                    urlService = "services/negotiation/statementBmf/{account}/FINAN/{dateStart}/{dateEnd}";
                }

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddUrlSegment("account", account);
                _request.AddUrlSegment("dateStart", dateStart.ToLower());
                _request.AddUrlSegment("dateEnd", dateEnd.ToLower());

                LogMessageRequest(_request, tokenJWT);
                //IRestResponse response = await GetResponseContentAsync(_client, _request);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region AuthenticationToken
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="tokenJWT"></param>
        /// <returns></returns>
        public async Task<AuthenticationResponseModel> AuthenticationToken(string ip, MessageTokenJWT tokenJWT)
        {
            try
            {
                if (ip.Equals("::1"))
                {
                    ip = "127.0.0.1";
                }

                string urlService = "";
                // /token/authentication?login=USER&password=PASS
                urlService = string.Format("token/authentication?login={0}&password={1}", _userWebFeeder, _passWebFeeder);
                _request = new RestRequest(urlService, Method.POST);

                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();


                if (response.ErrorException != null)
                {
                    throw new Exception("Usuário ou senha incorretos.");
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                // {"success":true,"token":"27cea59f-1040-4239-bc77-258b9bfe1084"}
                AuthenticationResponseModel jsonResponse = new AuthenticationResponseModel();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<AuthenticationResponseModel>(response.Content);
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }

                return jsonResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region LoginHomeBroker | LogoutHomeBroker | ChangePasswordHomeBroker | ForgotPasswordHomeBroker
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="ipAddress"></param>
        /// <param name="tokenJWT"></param>
        /// <param name="execReconnect"></param>
        /// <returns></returns>
        public async Task<InternalLoginResponseModel> LoginHomeBroker(string username, string password, string ipAddress, MessageTokenJWT tokenJWT, bool execReconnect = true)
        {
            string urlService = MODULO_NEGOTIATION + "brokerServiceLogin/{username}/{password}/{ip}/{appName}";

            try
            {
                LogMessage(string.Format("InternalLogin [{0}]", username), ipAddress);

                if (ipAddress.Equals("::1"))
                {
                    ipAddress = "127.0.0.1";
                }

                urlService = MODULO_NEGOTIATION + string.Format("brokerServiceLogin/{0}/{1}/{2}/{3}", username, password, ipAddress, _source);

                _request = new RestRequest(urlService, Method.GET);
                // { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();


                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                    if (response.StatusCode == HttpStatusCode.Unauthorized && execReconnect)
                    {
                        var responseLogin = await this.RefreshTokenAsync(ipAddress);
                        return await this.LoginHomeBroker(username, password, ipAddress, tokenJWT, false);
                    }
                }

                LogMessage(response.Content, ipAddress);

                // { "authenticationReqId":"20100","isAuthenticated":"N","text":"Senha e/ou usuário bloqueados. Entre em contato com seu supervisor."}
                InternalLoginResponseModel jsonResponse = new InternalLoginResponseModel();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<InternalLoginResponseModel>(response.Content);
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }

                return jsonResponse;
            }
            catch (Exception ex)
            {
                string messageError = string.Format("{0} - {1}", urlService, ex.Message);
                LogMessage(messageError, ipAddress);
                throw ex;
            }
        }

        public async Task<string> LogoutHomeBroker(string username, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                if (ipAddress.Equals("::1"))
                {
                    ipAddress = "127.0.0.1";
                }

                // string urlService = MODULO_NEGOTIATION + "brokerServiceLogout/{username}/{ip}";
                string urlService = MODULO_NEGOTIATION + string.Format("brokerServiceLogout/{0}/{1}", username, ipAddress);

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                //if (!string.IsNullOrEmpty(username))
                //{
                //    _request.AddUrlSegment("username", username);
                //}

                //if (ipAddress.Equals("::1"))
                //{
                //    _request.AddUrlSegment("ip", "127.0.0.1");
                //}
                //else
                //{
                //    _request.AddUrlSegment("ip", ipAddress);
                //}


                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();


                if (response.ErrorException != null)
                {
                    throw new Exception("Ocorreu um erro ao efetuar o Logout.");
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> ChangePasswordHomeBroker(string username, string password, string newPassword, string ipAddress, MessageTokenJWT tokenJWT)
        {
            string urlService = MODULO_NEGOTIATION + "brokerServiceChangePassword/{username}/{password}/{ip}/{newPassword}";

            try
            {
                if (ipAddress.Equals("::1"))
                {
                    ipAddress = "127.0.0.1";
                }

                urlService = MODULO_NEGOTIATION + string.Format("brokerServiceChangePassword/{0}/{1}/{2}/{3}", username, password, ipAddress, newPassword);

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                //if (!string.IsNullOrEmpty(username))
                //{
                //    _request.AddUrlSegment("username", username);
                //}

                //if (!string.IsNullOrEmpty(password))
                //{
                //    _request.AddUrlSegment("password", password);
                //}

                //if (ipAddress.Equals("::1"))
                //{
                //    _request.AddUrlSegment("ip", "127.0.0.1");
                //}
                //else
                //{
                //    _request.AddUrlSegment("ip", ipAddress);
                //}

                //if (!string.IsNullOrEmpty(newPassword))
                //{
                //    _request.AddUrlSegment("newPassword", newPassword);
                //}


                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.ErrorException != null)
                {
                    throw new Exception("Usuário ou senha incorretos.");
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                return response.Content;
            }
            catch (Exception ex)
            {
                string messageError = string.Format("{0} - {1}", urlService, ex.Message);
                LogMessage(messageError, ipAddress);

                throw ex;
            }
        }

        public async Task<string> ForgotPasswordHomeBroker(string username, string clientDocument, string ipAddress, MessageTokenJWT tokenJWT)
        {
            string urlService = MODULO_NEGOTIATION + "brokerServiceForgotPassword/{username}/{clientDocument}";

            try
            {
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                if (!string.IsNullOrEmpty(username))
                {
                    _request.AddUrlSegment("username", username);
                }

                if (!string.IsNullOrEmpty(clientDocument))
                {
                    _request.AddUrlSegment("clientDocument", clientDocument);
                }


                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.ErrorException != null)
                {
                    throw new Exception("Usuário ou senha incorretos.");
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                return response.Content;

            }
            catch (Exception ex)
            {
                string messageError = string.Format("{0} - {1}", urlService, ex.Message);
                LogMessage(messageError, ipAddress);

                throw ex;
            }
        }

        #endregion

        #region GetWarrantyList
        /// <summary>
        /// 
        /// </summary>
        /// <param name="account"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public async Task<string> GetWarrantyList(string account, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/negotiation/warrantyList/{account}";

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddUrlSegment("account", account);

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);
                //IRestResponse response = new RestResponse();
                //Task.Run(async () =>
                //{
                //    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                //}).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetWarrantyOperation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="account"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public async Task<string> GetWarrantyOperation(string account, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/negotiation/warrantyOperation/{account}";

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddUrlSegment("account", account);

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);
                //IRestResponse response = new RestResponse();
                //Task.Run(async () =>
                //{
                //    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                //}).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region SendWarrantyOperationUpdate

        public async Task<string> SendWarrantyOperationUpdate(
            string account,
            string warrantyID,
            string descWarranty,
            string quantity,
            string valueWarranty,
            string operation,
            string ipAddress,
            MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = MODULO_NEGOTIATION + "warrantyOperationUpdate/";
                _request = new RestRequest(urlService, Method.POST) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddQueryParameter("account", account);
                _request.AddQueryParameter("descWarranty", descWarranty);
                _request.AddQueryParameter("quantity", quantity);
                _request.AddQueryParameter("valueWarranty", valueWarranty);
                _request.AddQueryParameter("operation", operation);
                _request.AddQueryParameter("warrantyID", warrantyID);

                _request.AddQueryParameter("sourceaddress", ipAddress + "#" + _source);

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = await GetResponseContentAsync(_client, _request, tokenJWT);
                //IRestResponse response = new RestResponse();
                //Task.Run(async () =>
                //{
                //    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                //}).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #endregion


        #region Metodo - services/news

        #region GetNewsByDate

        public async Task<List<NewsResponseModel>> GetNewsByDate(string initialTime, string finalTime, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                //string urlService = MODULO_NEWS + "newsByDate/" + query;
                //_request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                //var dateStartTemp = DateTime.Now.AddMinutes(-15);
                //var dateEndlTemp = DateTime.Now.Date.AddDays(1).AddMilliseconds(-1);

                var dateStartTemp = initialTime + DateTime.Now.AddMinutes(-15).ToString("HHmmss");
                var dateEndlTemp = finalTime + DateTime.Now.Date.AddDays(1).AddMilliseconds(-1).ToString("HHmmss");
                var query = dateStartTemp + "/" + dateEndlTemp;

                string urlService = "";
                urlService = MODULO_NEWS + string.Format("newsByDate/{0}", query);
                _request = new RestRequest(urlService, Method.GET);

                LogMessageRequest(_request, tokenJWT);
                //IRestResponse response = await GetResponseContentAsync(_client, _request);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                //if (response.ErrorException != null)
                //{
                //    throw new Exception("Erro em obter notícias do dia.", response.ErrorException);
                //}

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                List<NewsResponseModel> jsonResponse = new List<NewsResponseModel>();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<List<NewsResponseModel>>(response.Content);
                    if (jsonResponse.Count > 0)
                    {
                        jsonResponse = jsonResponse.Where(w => w.description.Length > 5).ToList();
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }
                return jsonResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetNewsRelevantFacts

        public async Task<List<NewsResponseModel>> GetNewsRelevantFacts(string initialTime, string finalTime, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                var dateStartTemp = initialTime + DateTime.Now.AddMinutes(-15).ToString("HHmmss");
                var dateEndlTemp = finalTime + DateTime.Now.Date.AddDays(1).AddMilliseconds(-1).ToString("HHmmss");
                var query = dateStartTemp + "/" + dateEndlTemp;

                string urlService = MODULO_NEWS + "newsRelevantFacts/" + query;
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                //IRestResponse response = await GetResponseContentAsync(_client, _request);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                //if (response.ErrorException != null)
                //{
                //    throw new Exception("Erro em obter notícias do dia.", response.ErrorException);
                //}

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                List<NewsResponseModel> jsonResponse = new List<NewsResponseModel>();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<List<NewsResponseModel>>(response.Content);
                    if (jsonResponse.Count > 0)
                    {
                        jsonResponse = jsonResponse.Where(w => w.description.Length > 5).ToList();
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }
                return jsonResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetNewsByDatePagination

        public async Task<NewsPaginationModel> GetNewsByDatePagination(string initialTime, string finalTime, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                var dateStartTemp = initialTime + DateTime.Now.AddMinutes(-15).ToString("HHmmss");
                var dateEndlTemp = finalTime + DateTime.Now.Date.AddDays(1).AddMilliseconds(-1).ToString("HHmmss");
                var query = dateStartTemp + "/" + dateEndlTemp;

                string urlService = MODULO_NEWS + "newsByDatePagination/" + query;
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                //IRestResponse response = await GetResponseContentAsync(_client, _request);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                //if (response.ErrorException != null)
                //{
                //    throw new Exception("Erro em obter notícias do dia.", response.ErrorException);
                //}

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                NewsPaginationModel jsonResponse = new NewsPaginationModel();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<NewsPaginationModel>(response.Content);
                    if (jsonResponse != null && jsonResponse.list != null && jsonResponse.list.Count > 0)
                    {
                        jsonResponse.list = jsonResponse.list.Where(w => w.description.Length > 5).ToList();
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }
                return jsonResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region GetNewsSearchByDate

        public async Task<List<NewsResponseModel>> GetNewsSearchByDate(string text, string agency, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "";
                var dateStartTemp = DateTime.Now.ToString("ddMMyyyy") + "000000";
                var dateEndlTemp = DateTime.Now.ToString("ddMMyyyy") + DateTime.Now.Date.AddDays(1).AddMilliseconds(-1).ToString("HHmmss");
                var query = dateStartTemp + "/" + dateEndlTemp;
                var agencyCode = 0;

                if (agency.Equals("AGENCIA BOVESPA"))
                {
                    urlService = MODULO_NEWS + string.Format("newsByDate/{0}", query);
                    agencyCode = 3;
                }
                else if (agency.Equals("AGENCIA BMF"))
                {
                    urlService = MODULO_NEWS + string.Format("newsByDate/{0}", query);
                    agencyCode = 4;
                }
                else if (agency.Equals("FR"))
                {
                    urlService = MODULO_NEWS + string.Format("newsRelevantFacts/{0}", query);
                }
                else
                {
                    urlService = MODULO_NEWS + string.Format("newsByDate/{0}", query);
                }

                _request = new RestRequest(urlService, Method.GET);

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();
                
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                List<NewsResponseModel> jsonResponse = new List<NewsResponseModel>();
                try
                {
                    jsonResponse = JsonConvert.DeserializeObject<List<NewsResponseModel>>(response.Content);
                    if (jsonResponse.Count > 0)
                    {
                        if (agencyCode == 3 || agencyCode == 4)
                        {
                            jsonResponse = jsonResponse
                                            .Where(w => w.description.Length > 5 && w.description.Contains(text) && w.agencyCode == agencyCode)
                                            .Take(500)
                                            .OrderByDescending(c => c.newsDate)
                                            .ToList();
                        }
                        else
                        {
                            jsonResponse = jsonResponse
                                            .Where(w => w.description.Length > 5 && w.description.Contains(text))
                                            .Take(500)
                                            .OrderByDescending(c => c.newsDate)
                                            .ToList();
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao deserializar contéudo: " + response.Content, ex);
                }
                return jsonResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #endregion

        #region Metodo - GetFinantialRatios

        /// <summary>
        /// Retorna índices
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public async Task<List<QuoteResponseModel>> GetFinantialRatios(string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                List<QuoteResponseModel> retornoList = new List<QuoteResponseModel>();

                var retornoIPCA = await GetQuote("ipca", ipAddress, tokenJWT);
                var retornoIGPM = await GetQuote("igpm", ipAddress, tokenJWT);
                var retornoSELIC = await GetQuote("selic", ipAddress, tokenJWT);
                var retornoCDI = await GetQuote("cdi_over", ipAddress, tokenJWT);

                retornoList.Add(retornoIPCA);
                retornoList.Add(retornoIGPM);
                retornoList.Add(retornoSELIC);
                retornoList.Add(retornoCDI);

                return retornoList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Metodo - GetExchange

        /// <summary>
        /// Retorna informações câmbio
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public async Task<List<QuoteResponseModel>> GetExchange(string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                List<QuoteResponseModel> retornoList = new List<QuoteResponseModel>();

                var retornoDolar = await GetQuote("usd", ipAddress, tokenJWT);
                var retornoEuro = await GetQuote("eur", ipAddress, tokenJWT);
                var retornoPound = await GetQuote("gbp", ipAddress, tokenJWT);
                var retornoEuroXDolar = await GetQuote("eurusd", ipAddress, tokenJWT);

                retornoList.Add(retornoDolar);
                retornoList.Add(retornoEuro);
                retornoList.Add(retornoPound);
                retornoList.Add(retornoEuroXDolar);

                return retornoList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region Metodo - GetCommodities

        /// <summary>
        /// Retorna informações commodities
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public async Task<List<QuoteResponseModel>> GetCommodities(string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                List<QuoteResponseModel> retornoList = new List<QuoteResponseModel>();

                var retornoGold = await GetQuote("oz1d", ipAddress, tokenJWT);
                var retornoPetro = await GetQuote("wtiX", ipAddress, tokenJWT);
                var retornoCoffee = await GetQuote("icfY", ipAddress, tokenJWT);
                var retornoSoy = await GetQuote("sjcZ", ipAddress, tokenJWT);

                retornoList.Add(retornoGold);
                retornoList.Add(retornoPetro);
                retornoList.Add(retornoCoffee);
                retornoList.Add(retornoSoy);

                return retornoList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region TryError
        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <param name="urlService"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        private string TryError(IRestResponse response, string urlService, MessageTokenJWT tokenJWT)
        {
            var error = new
            {
                IP = tokenJWT.ipRequest,
                Account = tokenJWT.account,
                Method = response.Request.Method,
                HashCode = response.Request.GetHashCode(),
                Resource = response.Request.Resource,
                StatusCode = (int)response.StatusCode,
                Status = response.StatusCode,
                Content = response.Content
            };

            string messageError = string.Format("[{4}][{5}] {0} {1} | StatusCode: {2} - {3}",
                response.Request.Method,
                response.Request.Resource,
                (int)response.StatusCode,
                response.StatusCode,
                tokenJWT.ipRequest,
                tokenJWT.account);

            Console.Error.WriteLine(messageError);
            Log.Error("Error: {@Error}", error);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                lock (_client.CookieContainer)
                {
                    this.RefreshTokenSync(tokenJWT.ipRequest);
                }
                //var responseLogin = await this.RefreshTokenAsync(ipAddress);
            }
            if (response.ErrorException != null)
            {
                throw new Exception("StatusCode=" + Convert.ToString((int)response.StatusCode) + " - " + response.StatusDescription, response.ErrorException);
            }
            throw new Exception("StatusCode=" + Convert.ToString((int)response.StatusCode) + " - " + response.StatusDescription);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ipAddress"></param>
        private void LogMessageRequest(RestRequest request, MessageTokenJWT tokenJWT) // string ipAddress)
        {
            string messageInfo = string.Format("[{2}][{3}] {0} {1}", request.Method, request.Resource, tokenJWT.ipRequest, tokenJWT.account);
            Console.WriteLine(messageInfo);

            if (request.Parameters.Count > 0)
            {
                var info = new
                {
                    IP = tokenJWT.ipRequest,
                    Account = tokenJWT.account,
                    Method = request.Method,
                    HashCode = request.GetHashCode(),
                    Resource = request.Resource,
                    Parameters = request.Parameters
                };
                Log.Warning("Request: {@Info}", info);
            }
            else
            {
                var info = new
                {
                    IP = tokenJWT.ipRequest,
                    Account = tokenJWT.account,
                    Method = request.Method,
                    HashCode = request.GetHashCode(),
                    Resource = request.Resource,
                    Parameters = ""
                };
                Log.Warning("Request: {@Info}", info);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageInfo"></param>
        /// <param name="ipAddress"></param>
        private void LogMessage(string messageInfo, string ipAddress)
        {
            messageInfo = string.Format("[{0}] {1}", ipAddress, messageInfo);
            Console.WriteLine(messageInfo);
            Log.Warning(messageInfo);
        }

        #endregion


        #region Fixed Income - Renda Fixa

        //RfConsultaOferta
        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public async Task<string> GetFixedIncomeOffers(string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = MODULO_NEGOTIATION + "fixedIncomeList";
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                LogMessageRequest(_request, tokenJWT);
                //IRestResponse response = await GetResponseContentAsync(_client, _request);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.ErrorException != null)
                {
                    throw new Exception("Erro em obter as informações de renda fixa.", response.ErrorException);
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //List<FixedIncomeOffersModel> lstResponse = new List<FixedIncomeOffersModel>();

            //FixedIncomeOffersModel objLst;

            //for (int i = 0; i < 30; i++)
            //{

            //    objLst = new FixedIncomeOffersModel();

            //    objLst.offerNumber = i.ToString();
            //    objLst.issuerName = "Teste 0: " + i.ToString();

            //    if (i < 10)
            //    {
            //        objLst.productName = "CDB";
            //        objLst.description = "CDB BANCO PINE";
            //        objLst.minimumValue = "R$ 1.304,00";
            //        objLst.valorizationIndexPercentage = "120% " + objLst.productName;
            //        objLst.remuneration = "102% do CDB";

            //    }
            //    else if (i >= 11 && i <= 20)
            //    {
            //        objLst.productName = "LCI";
            //        objLst.description = "LCI BANCO PINE";
            //        objLst.minimumValue = "R$ 900,00";
            //        objLst.valorizationIndexPercentage = "150% " + objLst.productName;
            //        objLst.remuneration = "102% do LCI";
            //    }
            //    else if (i >= 21 && i <= 30)
            //    {
            //        objLst.productName = "LFT";
            //        objLst.description = "LFT BANCO PINE";
            //        objLst.minimumValue = "R$ 500,00";
            //        objLst.valorizationIndexPercentage = "100% " + objLst.productName;
            //        objLst.remuneration = "102% do LFT";
            //    }
            //    //else if (i >= 73 && i <= 96)
            //    //{
            //    //    objLst.productName = "CFT";
            //    //    objLst.description = "CFT BANCO PINE";
            //    //    objLst.minimumValue = "R$ 7.000,00";
            //    //    objLst.valorizationIndexPercentage = "100% " + objLst.productName;
            //    //    objLst.remuneration = "102% do CFT";
            //    //}

            //    //else if (i >= 97 && i <= 120)
            //    //{
            //    //    objLst.productName = "LCA PRE";
            //    //    objLst.description = "LCA PRE BANCO PINE";
            //    //    objLst.minimumValue = "R$ 8.000,00";
            //    //    objLst.valorizationIndexPercentage = "100% " + objLst.productName;
            //    //    objLst.remuneration = "102% do LCA";
            //    //}

            //    //else if (i >= 121 && i <= 145)
            //    //{
            //    //    objLst.productName = "COE";
            //    //    objLst.description = "COE BANCO PINE";
            //    //    objLst.minimumValue = "R$ 10.000,00";
            //    //    objLst.valorizationIndexPercentage = "100% " + objLst.productName;
            //    //    objLst.remuneration = "102% do COE";
            //    //}

            //    //else if (i >= 149 && i <= 173)
            //    //{
            //    //    objLst.productName = "CDCA";
            //    //    objLst.description = "CDCA BANCO PINE";
            //    //    objLst.minimumValue = "R$ 7.000,00";
            //    //    objLst.valorizationIndexPercentage = "100% " + objLst.productName;
            //    //    objLst.remuneration = "102% do CDCA";
            //    //}

            //    //else if (i >= 174 && i <= 199)
            //    //{
            //    //    objLst.productName = "CRI FLU";
            //    //    objLst.description = "CRI FLU BANCO PINE";
            //    //    objLst.minimumValue = "R$ 7.000,00";
            //    //    objLst.valorizationIndexPercentage = "100% " + objLst.productName;
            //    //    objLst.remuneration = "102% do CRI FLU";
            //    //}
            //    //else if (i >= 200 && i <= 223)
            //    //{
            //    //    objLst.productName = "LTN";
            //    //    objLst.description = "LTN BANCO PINE";
            //    //    objLst.minimumValue = "R$ 7.000,00";
            //    //    objLst.valorizationIndexPercentage = "100% " + objLst.productName;
            //    //    objLst.remuneration = "102% do LTN";
            //    //}
            //    //else if (i >= 224 && i <= 244)
            //    //{
            //    //    objLst.productName = "CDB PRE";
            //    //    objLst.description = "CDB PRE BANCO PINE";
            //    //    objLst.minimumValue = "R$ 7.000,00";
            //    //    objLst.valorizationIndexPercentage = "100% " + objLst.productName;
            //    //    objLst.remuneration = "102% do CDB PRE";
            //    //}



            //    //else if (i >= 245 && i <= 265)
            //    //{
            //    //    objLst.productName = "CRI SREMU";
            //    //    objLst.description = "LFT_2 BANCO PINE";
            //    //    objLst.minimumValue = "R$ 7.000,00";
            //    //    objLst.valorizationIndexPercentage = "100% " + objLst.productName;
            //    //    objLst.remuneration = "102% do CRI SREMU";
            //    //}

            //    //else if (i >= 266 && i <= 290)
            //    //{
            //    //    objLst.productName = "LH";
            //    //    objLst.description = "LH BANCO PINE";
            //    //    objLst.minimumValue = "R$ 7.000,00";
            //    //    objLst.valorizationIndexPercentage = "100% " + objLst.productName;
            //    //    objLst.remuneration = "102% do LH";
            //    //}

            //    //else if (i >= 291 && i <= 315)
            //    //{
            //    //    objLst.productName = "CDB POS";
            //    //    objLst.description = "CDB POS BANCO PINE";
            //    //    objLst.minimumValue = "R$ 7.000,00";
            //    //    objLst.valorizationIndexPercentage = "100% " + objLst.productName;
            //    //    objLst.remuneration = "102% do CDB POS";
            //    //}

            //    //else if (i >= 316 && i <= 330)
            //    //{
            //    //    objLst.productName = "CDB PRE DC";
            //    //    objLst.description = "CDB PRE DC BANCO PINE";
            //    //    objLst.minimumValue = "R$ 7.000,00";
            //    //    objLst.valorizationIndexPercentage = "100% " + objLst.productName;
            //    //    objLst.remuneration = "102% do CDB PRE DC";
            //    //}


            //    objLst.liquidity = "Diária";
            //    objLst.gracePeriod = "6 meses";

            //    objLst.unitPrice = "R$ 1.000,00";
            //    objLst.FGCCoverage = "Sim";
            //    objLst.rating = "BBB + (Standard & Poor) A - (Fitch)";

            //    objLst.issuingDate = DateTime.Today.ToString();
            //    objLst.expiryDate = DateTime.Today.AddDays(365).ToString("dd/MM/yyyy");

            //    objLst.deadline = "365 dias";

            //    objLst.riskDegree = "111";


            //    objLst.correctionIndex = "444";
            //    objLst.interestRate = "555";
            //    objLst.isVisible = "true";
            //    objLst.validationDate = DateTime.Today.AddDays(30).ToString();
            //    //objLst.description = "Tesouro Prefixado 2023 (LTN)";

            //    lstResponse.Add(objLst);
            //}

            //return lstResponse;
        }

        //RfPosicaoClientePorAplic
        public async Task<ClientPositionReturnModel> GetClientPositionPerApplication(string query, string ipAddress)
        {

            return new ClientPositionReturnModel()
            {
                entryNumber = "Número do lançamento",
                titleName = "Nome do Titulo",
                assetName = "Nome do papel",
                assetCode = "Código do Papel",
                expiryDate = "Data de Vencimento",
                cetipCode = "Código Cetip",
                emitterName = "Nome do Emissor",
                applicationDate = "Data do movimento",
                applicationPU = "PU de aplicação",
                operationCode = "Código da operação",
                operationDescription = "descrição da operação",
                quantity = "Quantidade",
                bloquedQuantity = "Quantidade bloqueada",
                currentPU = "PU atualizado",
                updatedValue = "valor atualizadao",
                incomeValue = "valor do rendimento",
                iRValue = "Valor de IR na data da consulta",
                iofValue = "Valor de IOF na data da consulta",
                netValue = "Valor líquido na data da consulta",
                renegotiationType = "Se paga juros 0=não;1=sim",
                decimalQuantity = "número de casas decimais",
                assetCurveName = "Nome da curva do papel"
            };
        }


        //RfEscreverResgatePreboleto
        public async Task<string> RegisterRescuePreBilletOrder(MessageFixedIncomeModel messageFixedIncome, string ipAddress)
        {
            // return new FixedIncomeResponseModel() { returningCode = "OK", operationNumber = "100", returnDescription = "Teste OK" };
            return "OK";
        }


        //RfAplicarOferta
        /// <summary>
        /// Registrar uma aplicação do cliente no pre-boleto.
        /// </summary>
        /// <param name="messageFixedIncome"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public async Task<string> ApplyFixedIncomeOffers(MessageFixedIncomeModel messageFixedIncome, string ipAddress)
        {

            //try
            //{
            //    string urlService = MODULO_NEGOTIATION + "cancelOrder/";
            //    _request = new RestRequest(urlService, Method.POST) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

            //    if (!string.IsNullOrEmpty(orderid))
            //    {
            //        _request.AddQueryParameter("orderid", orderid);
            //    }

            //    if (!string.IsNullOrEmpty(origclordid))
            //    {
            //        _request.AddQueryParameter("origclordid", origclordid);
            //    }

            //    if (!string.IsNullOrEmpty(quote))
            //    {
            //        _request.AddQueryParameter("quote", quote);
            //    }

            //    if (!string.IsNullOrEmpty(side))
            //    {
            //        _request.AddQueryParameter("side", side);
            //    }

            //    if (!string.IsNullOrEmpty(qtd))
            //    {
            //        _request.AddQueryParameter("qtd", qtd.Replace(".", ""));
            //    }

            //    if (!string.IsNullOrEmpty(username))
            //    {
            //        _request.AddQueryParameter("username", username);
            //    }

            //    if (!string.IsNullOrEmpty(market))
            //    {
            //        _request.AddQueryParameter("market", market);
            //    }

            //    _request.AddQueryParameter("sourceaddress", ipAddress + "#" + _source);

            //    LogMessageRequest(_request, tokenJWT);
            //    IRestResponse response = await GetResponseContentAsync(_client, _request);

            //    if (response.StatusCode != HttpStatusCode.OK)
            //    {
            //        TryError(response, urlService, tokenJWT);
            //    }
            //    return response.Content;
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            //getBrokeragePDF("", ipAddress);

            // FixedIncomeResponseModel response = new FixedIncomeResponseModel() { returningCode = "OK", operationNumber = "100", returnDescription = "Teste OK" };

            return "OK";
        }









        #endregion


        #region Brokerage 

        public async Task<byte[]> getBrokeragePDF(string account, string market, string date, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                var query = string.Concat(account, "/", market, "/", date);
                string urlService = string.Concat("services/negotiation/brokeragePDF/", query);
                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };

                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }

                return response.RawBytes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Tesouro Direto

        public async Task<List<DirectTreasureModel>> GetDirectTreasureOffers(string query, string ipAddress)
        {

            List<DirectTreasureModel> lstResponse = new List<DirectTreasureModel>();

            DirectTreasureModel objLst;

            for (int i = 0; i < 30; i++)
            {

                objLst = new DirectTreasureModel();

                if (i <= 10)
                {
                    objLst.treasureType = "PRÉ-FIXADO";
                    objLst.treasureName = "Tesouro Prefixado 2023 (LTN)";
                    objLst.percentage = "10.4% a.a.";
                    objLst.unitPrice = "1.304,00";
                    objLst.expiryDate = "05/02/2023";

                    objLst.minimalApplication = "30,00";
                    objLst.remuneration = "9,99% ao ano";
                    objLst.coupon = "Não há";

                    objLst.investmentValue = "15.000,00";
                    objLst.quantity = "1000";
                    objLst.operationalTax = "19,00";
                    objLst.liquidTotal = "14.800,00";
                }
                else if (i >= 11 && i <= 20)
                {
                    objLst.treasureType = "PÓS FIXADO";
                    objLst.treasureName = "Tesouro Posfixado 2023 (LTN)";
                    objLst.percentage = "10.4% a.a.";
                    objLst.unitPrice = "1.304,00";
                    objLst.expiryDate = "05/02/2023";

                    objLst.minimalApplication = "30,00";
                    objLst.remuneration = "9,99% ao ano";
                    objLst.coupon = "Não há";

                    objLst.investmentValue = "15.000,00";
                    objLst.quantity = "1000";
                    objLst.operationalTax = "19,00";
                    objLst.liquidTotal = "14.800,00";

                }
                else if (i >= 21 && i <= 30)
                {
                    objLst.treasureType = "INFLAÇÃO";
                    objLst.treasureName = "Tesouro Inflação 2023 (LTN)";
                    objLst.percentage = "10.4% a.a.";
                    objLst.unitPrice = "1.304,00";
                    objLst.expiryDate = "05/02/2023";

                    objLst.minimalApplication = "30,00";
                    objLst.remuneration = "9,99% ao ano";
                    objLst.coupon = "Não há";

                    objLst.investmentValue = "15.000,00";
                    objLst.quantity = "1000";
                    objLst.operationalTax = "19,00";
                    objLst.liquidTotal = "14.800,00";
                }

                lstResponse.Add(objLst);
            }

            return lstResponse;
        }


        #endregion


        #region Fundos

        public async Task<List<FundsModel>> GetFundsOffers(string query, string ipAddress)
        {

            List<FundsModel> lstResponse = new List<FundsModel>();

            FundsModel objLst;

            for (int i = 0; i < 30; i++)
            {
                objLst = new FundsModel();

                if (i <= 10)
                {

                    objLst = FillObject(objLst, "MULTIMERCADO", i);
                }
                else if (i >= 11 && i <= 20)
                {
                    objLst = FillObject(objLst, "RENDA FIXA", i);

                }
                else if (i >= 21 && i <= 30)
                {
                    objLst = FillObject(objLst, "AÇÕES", i);
                }

                lstResponse.Add(objLst);
            }

            return lstResponse;
        }

        private FundsModel FillObject(FundsModel objLst, string fundsType, int index)
        {
            //GENERAL
            objLst.fundsType = fundsType;

            switch (fundsType)
            {
                case "MULTIMERCADO":
                    objLst.fundsName = "CLARITAS MULTIMERCADO";
                    break;

                case "RENDA FIXA":
                    objLst.fundsName = "CLARITAS RENDA FIXA";
                    break;

                case "AÇÕES":
                    objLst.fundsName = "CLARITAS AÇÕES";
                    break;
                default:
                    objLst.fundsName = "MULTIMERCADO";
                    break;
            }

            objLst.minimalApplication = "20.000,00";
            objLst.rentability = "13,7%";

            if (index % 2 == 0)
            {
                objLst.risk = "BAIXO";
            }
            else
            {
                objLst.risk = "MÉDIO";
            }

            //INFO
            objLst.ANBIMAClassification = "Multiestratégia multimercados";
            objLst.administrativeTax = "2,5%";
            objLst.administrativeTaxMax = "3,0%";
            objLst.performanceTax = "20% do que exceder o CDI";
            objLst.gracePeriod = "Não há";
            objLst.entranceTax = "Não há";
            objLst.exitTax = "Não há";
            objLst.applicationSubscription = "D+0";
            objLst.redemptionSubscription = "D+0";
            objLst.liquidRedemption = "D+4";
            objLst.minimumBalance = "5.000,00";
            objLst.minimumInicialApplication = "5.000,00";
            objLst.minimumMovimentation = "1.000,00";
            objLst.manager = "Lerosa Investimentos";
            objLst.administrator = "BNY Mellon";
            objLst.custodian = "Bradesco";

            //INVEST
            objLst.classification = "Renda Fixa";
            objLst.minimalInvestment = "10.000,00";
            objLst.redemptionLiquidity = "5.000,00";
            objLst.value = "1 dia útil";
            return objLst;
        }


        #endregion


        #region GetBrokerAccountList
        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <param name="ipAddress"></param>
        /// <param name="tokenJWT"></param>
        /// <returns></returns>
        public async Task<string> GetBrokerAccountList(string login, string ipAddress, MessageTokenJWT tokenJWT)
        {
            try
            {
                string urlService = "services/negotiation/brokerServiceAccountList/{login}";

                _request = new RestRequest(urlService, Method.GET) { RequestFormat = DataFormat.Json, DateFormat = "dd-MM-yyyy HH:mm:ss" };
                _request.AddUrlSegment("login", login);

                LogMessageRequest(_request, tokenJWT);
                IRestResponse response = new RestResponse();
                Task.Run(async () =>
                {
                    response = await GetResponseContentAsync(_client, _request, tokenJWT) as RestResponse;
                }).Wait();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    TryError(response, urlService, tokenJWT);
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
