﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using btg.homebroker.api.Auth;
using btg.homebroker.api.Model;
using btg.homebroker.api.Model.Auth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;
using Swashbuckle.AspNetCore.Swagger;

namespace btg.homebroker.api
{
    public class Startup
    {
        public string ContentRootPath { get; private set; }

        /// <summary>
        /// Construtor criado apenas repassar o HostingEnvironment para a classe base
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env)
        {
            var projectName = this.GetType().GetTypeInfo().Assembly.GetName().Name;
            ContentRootPath = env.ContentRootPath;
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(env.ContentRootPath);
            SetUpConfigurationBuilder(builder);
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();

            var configOptions = Configuration.GetSection(nameof(WebFeederOptions));
            var urlServerElasticSearch = configOptions[nameof(WebFeederOptions.UrlElasticSearch)];
            var indexServerElasticSearch = configOptions[nameof(WebFeederOptions.IndexElasticSearch)];

            Console.WriteLine($"ElasticSearch-Url: {urlServerElasticSearch}");
            Console.WriteLine($"ElasticSearch-Index: {indexServerElasticSearch}");
            if (urlServerElasticSearch == null)
            {
                urlServerElasticSearch = "localhost:9200";
                indexServerElasticSearch = "webfeeder-{0:yyyy.MM.dd}";
            }

            // For Serilog with ElasticSearch
            Log.Logger = new LoggerConfiguration()
              .MinimumLevel.Warning()
              .WriteTo.RollingFile(Path.Combine(env.ContentRootPath + "/logs", "log-{Date}.log"))
              .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(urlServerElasticSearch))
              {
                  MinimumLogEventLevel = LogEventLevel.Warning,
                  IndexFormat = indexServerElasticSearch,
                  AutoRegisterTemplate = true,
              })
              //.WriteTo.Seq("http://localhost:5341")
              .CreateLogger();
        }

        public IConfigurationRoot Configuration { get; }

        protected virtual void SetUpConfigurationBuilder(IConfigurationBuilder builder)
        {
            Console.WriteLine($"AppSettings: {ContentRootPath}/configs/appsettings.json");
            builder.AddJsonFile($"{ContentRootPath}/configs/appsettings.json", optional: true, reloadOnChange: true);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddSingleton<IJwtFactory, JwtFactory>();

            // Get options from app settings
            var configOptions = Configuration.GetSection(nameof(WebFeederOptions));
            // Get options from app settings
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

            Console.WriteLine($"WebFeeder-Url-01: {configOptions[nameof(WebFeederOptions.UrlWebFeeder_01)]}");
            // Console.WriteLine($"WebFeeder-Url-02: {configOptions[nameof(WebFeederOptions.UrlWebFeeder_02)]}");
            var _webFeederServiceInstance = new BLL.WebfeederServicesBLL(configOptions[nameof(WebFeederOptions.Source)],
                                                                         configOptions[nameof(WebFeederOptions.User)],
                                                                         configOptions[nameof(WebFeederOptions.Pass)],
                                                                         configOptions[nameof(WebFeederOptions.UrlWebFeeder_01)]
                                                                         // configOptions[nameof(WebFeederOptions.UrlWebFeeder_02)]
                                                                         );
            services.AddSingleton(_webFeederServiceInstance);

            var __tokenServiceInstance = new BLL.BTGBLL(configOptions[nameof(WebFeederOptions.UrlToken)],
                                                        jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],
                                                        jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],
                                                        jwtAppSettingOptions[nameof(JwtIssuerOptions.Key)],
                                                        configOptions[nameof(WebFeederOptions.UrlWebFeeder_01)],
                                                        // configOptions[nameof(WebFeederOptions.UrlWebFeeder_02)],
                                                        configOptions[nameof(WebFeederOptions.User)],
                                                        configOptions[nameof(WebFeederOptions.Pass)],
                                                        configOptions[nameof(WebFeederOptions.AccountTest)]);
            services.AddSingleton(__tokenServiceInstance);


            /////////////////////////////////////////////////////////////
            // jwt wire up
            // Configure JwtIssuerOptions
            string SecretKey = jwtAppSettingOptions[nameof(JwtIssuerOptions.Key)];
            SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });
            //// api user claim policy
            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy("Client", roles => roles.RequireClaim(Constants.Strings.JwtClaimIdentifiers.Rol, Constants.Strings.JwtClaims.Client));
            //    options.AddPolicy("Analyst", roles => roles.RequireClaim(Constants.Strings.JwtClaimIdentifiers.Rol, Constants.Strings.JwtClaims.Analyst));
            //    options.AddPolicy("Administrator", roles => roles.RequireClaim(Constants.Strings.JwtClaimIdentifiers.Rol, Constants.Strings.JwtClaims.Administrator));
            //});
            /////////////////////////////////////////////////////////////


            var pathToDoc = PlatformServices.Default.Application.ApplicationName + ".xml";

            // Versão atual da API
            var sVersion = "v1.0.2018_08_21";
#if BTG
            sVersion = sVersion + "_BTG";
#endif
#if VEXTER
            sVersion = sVersion + "_VEXTER";
#endif
#if LEROSA
            sVersion = sVersion + "_LEROSA";
#endif
#if FAST
            sVersion = sVersion + "_FAST";
#endif

            Console.WriteLine($"Version: {sVersion}");
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1",
                    new Info
                    {
                        Title = "API - WebFeeder - " + sVersion,
                        Version = sVersion,
                        Description = "Service to get information from webfeeder.",
                        TermsOfService = "None"
                    }
                 );

                var filePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, pathToDoc);
                options.IncludeXmlComments(filePath);
                options.DescribeAllEnumsAsStrings();
            });

            // Add framework services.
            services.AddMemoryCache();
            services.AddMvc();
            services.AddCors(options => options.AddPolicy("AllowCors", builder =>
             {
                 builder.WithOrigins("*")
                        .WithMethods("GET", "PUT", "POST", "DELETE") //AllowSpecificMethods;  
                        .AllowAnyHeader(); //AllowAllHeaders;  
             })
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            // loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            // loggerFactory.AddDebug();
            loggerFactory.AddSerilog();

            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swagger, httpReq) => swagger.Host = httpReq.Host.Value);
            });

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1 Docs");
                c.RoutePrefix = "swg";
            });


            ConfigureJwt(app);


            app.UseMvc();
            app.UseCors(builder =>
            {
                builder.WithOrigins("*");
                builder.AllowAnyOrigin();
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
            }
            );
        }

        private void ConfigureJwt(IApplicationBuilder app)
        {
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));
            //var tokenValidationParameters = new TokenValidationParameters
            //{
            //    ValidateIssuer = true,
            //    ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

            //    ValidateAudience = true,
            //    ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

            //    ValidateIssuerSigningKey = true,
            //    //IssuerSigningKey = _signingKey,

            //    RequireExpirationTime = false,
            //    ValidateLifetime = false,
            //    ClockSkew = TimeSpan.Zero
            //};

            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = new TokenValidationParameters
                {
                    // Validate the JWT Issuer (iss) claim
                    ValidateIssuer = true,
                    ValidIssuer = Configuration.GetSection("JwtIssuerOptions:Issuer").Value,
                    // Validate the JWT Audience (aud) claim
                    ValidateAudience = true,
                    ValidAudience = Configuration.GetSection("JwtIssuerOptions:Audience").Value,
                    // The signing key must match!
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetSection("JwtIssuerOptions:Key").Value)),
                    // Validate the token expiry
                    ValidateLifetime = true,
                    // If you want to allow a certain amount of clock drift, set that here:
                    // This defines the maximum allowable clock skew - i.e.
                    // provides a tolerance on the token expiry time 
                    // when validating the lifetime. As we're creating the tokens 
                    // locally and validating them on the same machines which 
                    // should have synchronised time, this can be set to zero. 
                    // Where external tokens are used, some leeway here could be 
                    // useful.
                    // ClockSkew = TimeSpan.Zero,
                },
                //Events = new CustomBearerEvents()
                //TokenValidationParameters = tokenValidationParameters
            });
        }

    }
}
