﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class NewsPaginationModel
    {
        public paginationInformation pagination { get; set; }
        public List<NewsResponseModel> list { get; set; }
    }

    public partial class paginationInformation
    {
        public Int64 page { get; set; }
        public Int64 size { get; set; }
        public Int64 lastNewsCode { get; set; }
        public Int64 lastPage { get; set; }
        public Int64 requestId { get; set; }
        public Int64 sizeTotal { get; set; }
    }
}
