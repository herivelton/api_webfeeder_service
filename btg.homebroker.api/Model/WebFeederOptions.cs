using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class WebFeederOptions
    {
        public string UrlElasticSearch { get; set; }
        public string IndexElasticSearch { get; set; }

        public string UrlToken { get; set; }
        public string UrlWebFeeder_01 { get; set; }
        // public string UrlWebFeeder_02 { get; set; }
        public string Source { get; set; }
        public string User { get; set; }
        public string Pass { get; set; }
        public string AccountTest { get; set; }
    }
}
