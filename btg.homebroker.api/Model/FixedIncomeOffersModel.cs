﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class FixedIncomeOffersModel
    {
        public string offerNumber { get; set; }

        public string issuerName { get; set; }

        public string productName { get; set; }

        public string issuingDate { get; set; }

        public string expiryDate { get; set; }

        public string riskDegree { get; set; }

        public string minimumValue { get; set; }

        public string valorizationIndexPercentage { get; set; }

        public string correctionIndex { get; set; }

        public string interestRate { get; set; }

        public string isVisible { get; set; }

        public string validationDate { get; set; }

        public string description { get; set; }

        public string deadline { get; set; }

        public string remuneration { get; set; }

        public string liquidity { get; set; }

        public string gracePeriod { get; set; }

        public string unitPrice { get; set; }

        public string FGCCoverage { get; set; }

        public string rating { get; set; }

    }

    public class ClientPositionReturnModel
    {

        public string entryNumber { get; set; }
        
        public string titleName { get; set; }

        public string assetName { get; set; }

        public string assetCode { get; set; }

        public string expiryDate { get; set; }

        public string cetipCode { get; set; }

        public string emitterName { get; set; }

        public string applicationDate { get; set; }

        public string applicationPU { get; set; }

        public string operationCode { get; set; }

        public string operationDescription { get; set; }

        public string quantity { get; set; }

        public string bloquedQuantity { get; set; }

        public string currentPU { get; set; }

        public string updatedValue { get; set; }

        public string incomeValue { get; set; }
        
        public string iRValue { get; set; }
        
        public string iofValue { get; set; }
        
        public string netValue { get; set; }
        
        public string renegotiationType { get; set; }

        public string decimalQuantity { get; set; }

        public string assetCurveName { get; set; }

    }
}


