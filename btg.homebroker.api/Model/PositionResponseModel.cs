﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{

    public class CurrentPosition
    {
        public class ExecutedBuy
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public class ExecutedSell
        {
            public string name { get; set; }
            public string value { get; set; }
        }


        public class OpenProfiLoss
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public class ClosedProfitLoss
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public class ProfitLoss
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public class LastTrade
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public class AveragePricePos
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public class Quote
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public Quote quote { get; set; }
        public ExecutedBuy executedBuy { get; set; }
        public ExecutedSell executedSell { get; set; }
        public OpenProfiLoss openProfitLoss { get; set; }
        public ClosedProfitLoss closedProfitLoss { get; set; }
        public decimal DayNet { get => decimal.Parse(executedBuy.value) - decimal.Parse(executedSell.value); }
        public AveragePricePos averagePricePos { get; set; }
        public ProfitLoss profitLoss { get; set; }
        public LastTrade lastTrade { get; set; }
        public bool isBmf { get; set; }
    }

    public class PositionResponseModel
    {
        public List<CurrentPosition> currentPosition { get; set; }
        public List<CurrentPosition> custodyBovespa { get; set; }
        public List<CurrentPosition> custodyBmf { get; set; }

        public List<CurrentPosition> MergeListsBmfBovespa()
        {
            List<CurrentPosition> listPosition = new List<CurrentPosition>();

            listPosition = this.custodyBovespa;
            if (this.custodyBmf != null) {
                foreach (CurrentPosition item in this.custodyBmf)
                {
                    CurrentPosition newItem = new CurrentPosition();
                    newItem.executedBuy = item.executedBuy;
                    newItem.executedSell = item.executedSell;
                    newItem.quote = item.quote;
                    if (item.averagePricePos.value.Contains("Pts."))
                    {
                        item.averagePricePos.value = Decimal.Parse(item.averagePricePos.value.Replace("Pts.", ""), new System.Globalization.CultureInfo("en-US")).ToString("N0");
                    }
                    newItem.averagePricePos = item.averagePricePos;
                    newItem.openProfitLoss = new CurrentPosition.OpenProfiLoss();
                    newItem.openProfitLoss.value = "0";
                    newItem.closedProfitLoss = new CurrentPosition.ClosedProfitLoss();
                    newItem.closedProfitLoss.value = "0";
                    newItem.isBmf = true;
                    listPosition.Add(newItem);
                }
            }

            return listPosition;
        }
    }

}