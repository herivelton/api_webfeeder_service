﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class NewsResponseModel
    {
        public Int64 newsCode { get; set; }
        public Int64 agencyCode { get; set; }
        public string agencyDescription { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public Int64 categoryCode { get; set; } 
        public DateTime newsDate { get; set; }
        
    }
}
