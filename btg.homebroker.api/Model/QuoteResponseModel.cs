﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class QuoteResponseModel
    {

        /// <summary>
        /// Model for a quote.
        /// </summary>
        public string Symbol { get; set; }
        public string TimeUpdate { get; set; }
        public decimal LastTrade { get; set; }
        //public decimal Previous { get; set; }
        public decimal Change { get; set; }
        //public decimal High { get; set; }
        //public decimal Low { get; set; }
        //public decimal Bid { get; set; }
        //public decimal Ask { get; set; }
        //public decimal Open { get; set; }
        //public decimal ChangeMonth { get; set; }
        //public decimal Average { get; set; }

        public decimal volumeFinancier { get; set; }
        public decimal volumeAmount { get; set; }
        public decimal quantityTrades { get; set; }

        public string Msg { get; set; }
    }

    public class QuoteQueryResponseModel
    {
        public string symbol { get; set; }
        public string company { get; set; }
    }
}
