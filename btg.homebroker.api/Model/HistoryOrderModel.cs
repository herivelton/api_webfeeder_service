﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class HistoryOrderModel
    {
        public string quote { get; set; }
        public string buyOrSell { get; set; }
        public string account { get; set; }
        public string state { get; set; }
        public string quantitySupplied { get; set; }
        public string quantityExecuted { get; set; }
        public string quantityRemaining { get; set; }
        public string price { get; set; }
        public string priceStop { get; set; }
        public string priceStopGain { get; set; }
        public string price2 { get; set; }
        public string movingStart { get; set; }
        public string initialChangeType { get; set; }
        public string initialChange { get; set; }
        public string pricePercentage { get; set; }
        public string maxFloor { get; set; }
        public string priceAverage { get; set; }
        public string type { get; set; }
        public string maturity { get; set; }
        public string market { get; set; }
        public string transactTime { get; set; }
        public string side { get; set; }
        public string currentCumQty { get; set; }
        public string currentAvgPx { get; set; }
        public string orderID { get; set; }
        public string clOrdID { get; set; }
        public string timeInForce { get; set; }
        public string username { get; set; }
        public string text { get; set; }
        public string sourceAddress { get; set; }
        public string orderStrategy { get; set; }
        public string soldQty { get; set; }
        public string boughtQty { get; set; }
        public string buyAvgPx { get; set; }
        public string sellAvgPx { get; set; }
        public string factor { get; set; }
    }


    //public class Quote
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class BuyOrSell
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class Account
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class State
    //{
    //    public string code { get; set; }
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class QuantitySupplied
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class QuantityExecuted
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class QuantityRemaining
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class Price
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class PriceStop
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class PriceStopGain
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class Price2
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class MovingStart
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class InitialChangeType
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class InitialChange
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class PricePercentage
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class MaxFloor
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class PriceAverage
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class Type
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class Maturity
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class Mercado
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class TransactTime
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class Side
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class CurrentCumQty
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class CurrentAvgPx
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class OrderID
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class ClOrdID
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class TimeInForce
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class Username
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class Text
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class SourceAddress
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class OrderStrategy
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class SoldQty
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class BoughtQty
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class BuyAvgPx
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class SellAvgPx
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class Factor
    //{
    //    public string name { get; set; }
    //    public string value { get; set; }
    //}

    //public class ListBean
    //{
    //    public Quote quote { get; set; }
    //    public BuyOrSell buyOrSell { get; set; }
    //    public Account account { get; set; }
    //    public State state { get; set; }
    //    public QuantitySupplied quantitySupplied { get; set; }
    //    public QuantityExecuted quantityExecuted { get; set; }
    //    public QuantityRemaining quantityRemaining { get; set; }
    //    public Price price { get; set; }
    //    public PriceStop priceStop { get; set; }
    //    public PriceStopGain priceStopGain { get; set; }
    //    public Price2 price2 { get; set; }
    //    public MovingStart movingStart { get; set; }
    //    public InitialChangeType initialChangeType { get; set; }
    //    public InitialChange initialChange { get; set; }
    //    public PricePercentage pricePercentage { get; set; }
    //    public MaxFloor maxFloor { get; set; }
    //    public PriceAverage priceAverage { get; set; }
    //    public Type type { get; set; }
    //    public Maturity maturity { get; set; }
    //    public Mercado mercado { get; set; }
    //    public TransactTime transactTime { get; set; }
    //    public Side side { get; set; }
    //    public CurrentCumQty currentCumQty { get; set; }
    //    public CurrentAvgPx currentAvgPx { get; set; }
    //    public OrderID orderID { get; set; }
    //    public ClOrdID clOrdID { get; set; }
    //    public TimeInForce timeInForce { get; set; }
    //    public Username username { get; set; }
    //    public Text text { get; set; }
    //    public SourceAddress sourceAddress { get; set; }
    //    public OrderStrategy orderStrategy { get; set; }
    //    public SoldQty soldQty { get; set; }
    //    public BoughtQty boughtQty { get; set; }
    //    public BuyAvgPx buyAvgPx { get; set; }
    //    public SellAvgPx sellAvgPx { get; set; }
    //    public Factor factor { get; set; }
    //}

    //public class HistoryOrderModel
    //{
    //    public List<ListBean> listBeans { get; set; }
    //}

}