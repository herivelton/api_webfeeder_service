﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class CandleTradingViewResponse
    {
        /// <summary>
        /// Array de data (timestap unix)
        /// </summary>
        public List<Int64> t { get; set; }
        /// <summary>
        /// Array do preço de fechamento
        /// </summary>
        public List<decimal> c { get; set; }
        /// <summary>
        /// Array do preço de abertura
        /// </summary>
        public List<decimal> o { get; set; }
        /// <summary>
        /// Array do preço de máxima
        /// </summary>
        public List<decimal> h { get; set; }
        /// <summary>
        /// Array do preço de mínima
        /// </summary>
        public List<decimal> l { get; set; }
        /// <summary>
        /// Array de volume
        /// </summary>
        public List<decimal> v { get; set; }
        public string s  { get; set; }

        public CandleTradingViewResponse() {
            t = new List<long>();
            c = new List<decimal>();
            o = new List<decimal>();
            h = new List<decimal>();
            l = new List<decimal>();
            v = new List<decimal>();
        }
    }

    public class CandleChartResponse
    {
        public string quote { get; set; }
        public string period { get; set; }
        public string tick_size { get; set; }
        public string error { get; set; }
        public string subscribe_id { get; set; }
        public List<CandleResponse> candles { get; set; }
    }

    // Quote            : Indica o ativo o qual foi pedido;
    // Period           : Indica o período o qual foi pedido;
    // Tick_size        : Quantidade de casas decimais do ativo retornado pelo difusor;
    // Response_type    : Tipo de comando no difusor, no caso gch;
    // Error            : Texto de erro, caso haja;
    // Subscribe_id     : Id de conexão com o difusor;
    // Candles[].date   : Data da cotação;
    // Candles[].price  : Valor do ativo na data;
    // Candles[].open   : Valor do ativo de abertura do mercado;
    // Candles[].high   : Maior valor do ativo na data;
    // Candles[].low    : Menor valor do ativo na data;
    // Candles[].fVol   : Volume financeiro;
    // Candles[].aVol   : Volume quantitativo;
    // Candles[].isClose: Determina se o candle está fechado.

    public class CandleResponse
    {
        public Int64 date { get; set; }
        public decimal price { get; set; }
        public decimal open { get; set; }
        public decimal high { get; set; }
        public decimal low { get; set; }

        public decimal fVol { get; set; }
        public decimal aVol { get; set; }
        public bool isClose { get; set; }
    }
}


    //"quote": "DOLH18",
    //"period": "10",
    //"tick_size": "",
    //"response_type": "gch",
    //"error": "",
    //"subscribe_id": "aaabbb",
    //"candles": [
    // {
    //"date": 201802160900,
    //"price": 3236,
    //"open": 3245,
    //"high": 3245.5,
    //"low": 3235.5,
    //"fVol": "29406360.0",
    //"aVol": "9075",
    //"isClose": true
    // }