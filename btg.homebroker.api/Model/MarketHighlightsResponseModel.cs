﻿using System;
using System.Collections.Generic;
using System.Text;

namespace btg.homebroker.api.Model
{
    public class MarketHighlightsResponseModel
    {
        public string quote { get; set; }
        public decimal price { get; set; }
        public decimal value { get; set; }
    }
}
