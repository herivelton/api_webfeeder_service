﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class FundsModel
    {
        //GENERAL
        public string fundsType { get; set; }
        public string fundsName { get; set; }
        public string minimalApplication { get; set; }
        public string rentability { get; set; }
        public string risk { get; set; }


        //INFO
        public string ANBIMAClassification { get; set; }
        public string administrativeTax { get; set; }
        public string administrativeTaxMax { get; set; }
        public string performanceTax { get; set; }
        public string gracePeriod { get; set; }
        public string entranceTax { get; set; }
        public string exitTax { get; set; }
        public string applicationSubscription { get; set; }
        public string redemptionSubscription { get; set; }
        public string liquidRedemption { get; set; }
        public string minimumBalance { get; set; }
        public string minimumInicialApplication { get; set; }
        public string minimumMovimentation { get; set; }
        public string manager { get; set; }
        public string administrator { get; set; }
        public string custodian { get; set; }

        //INVEST
        public string classification { get; set; }
        public string minimalInvestment { get; set; }
        public string redemptionLiquidity { get; set; }
        public string value { get; set; }
    }
}
