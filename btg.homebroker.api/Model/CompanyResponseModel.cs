﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class CompanyResponseModel
    {
        public int codeLocal { get; set; }
        public int marketCode { get; set; }
        public string name { get; set; }
        public string marketName { get; set; }
        public int segmentCode { get; set; }
    }
}
