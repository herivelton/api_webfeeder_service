﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class MessageHistoryOrderModel
    {
        public string account { get; set; }

        public string market { get; set; }

        public string dataInicio { get; set; }

        public string dataFim { get; set; }

        public string symbol { get; set; }

        public string status { get; set; }

        public string direction { get; set; }

        public string orderID { get; set; }

        public string type { get; set; }

        public string statusGroup { get; set; }

        public string queryType { get; set; }
    }
}
