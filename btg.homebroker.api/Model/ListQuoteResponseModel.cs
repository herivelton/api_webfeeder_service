﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class ListQuoteResponseModel
    {
        public string description { get; set; }
        public string speak { get; set; }
        public string isinPaper { get; set; }
        public string expirationDate { get; set; }
        public DateTime? expiration
        {
            get
            {
                DateTime dateExp;
                if (DateTime.TryParse(this.expirationDate, out dateExp))
                {
                    return dateExp;
                }
                return null;
            }
        }
        public string marketName { get; set; }
        public string typeQuotesDescription { get; set; }
        public string empresa { get; set; }
        public string ativoObjeto { get; set; }
        public double precoExercicio { get; set; }


        // public string marketCode { get; set; }
        // public string quotesLotDefault { get; set; }
        // public string quotesQuotationForm { get; set; }
        // public string quotesIsinPaper { get; set; }
        // public string companyName { get; set; }
        // public string segmentName { get; set; }
        // public string typeQuotesCode { get; set; }
        // public string classificationName { get; set; }
        // public string quotesCurrencyDescription { get; set; }

    }
}
