﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class DirectTreasureModel
    {
        //GENERAL
        public string treasureType { get; set; }
        public string treasureName { get; set; }
        public string percentage { get; set; }
        public string unitPrice { get; set; }
        public string expiryDate { get; set; }

        //INFO
        public string minimalApplication { get; set; }
        public string remuneration { get; set; }
        public string coupon { get; set; }

        //INVEST
        public string investmentValue { get; set; }
        public string quantity { get; set; }

        public string operationalTax { get; set; } 

        public string liquidTotal { get; set; }
    }
}
