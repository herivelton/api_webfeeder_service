﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class MessageFixedIncomeModel
    {
        public string offerNumber { get; set; }

        public string ClientCode { get; set; }

        public string OperationDate { get; set; }

        public string OperationQuantity { get; set; }

        public string OperationValue { get; set; }

        public string OperationUser { get; set; }

    }
}
