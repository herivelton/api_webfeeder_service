﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class MessageUserHomeBroker
    {
        public string username { get; set; }

        public string password { get; set; }

        public string newPassword { get; set; }

        public string clientDocument { get; set; }
    }

    public class MessageValideToken
    {
        public string account { get; set; }

        public string tokenA { get; set; }

        public string tokenW { get; set; }
    }

    public class MessageUserCache
    {
        public string account { get; set; }
        public string guid { get; set; }
        public string ipRequest { get; set; }
        public DateTime dateIn { get; set; }
        public DateTime? dateUpdate { get; set; }
    }

    public class MessageTokenJWT
    {
        public string account { get; set; }

        public string tokenInterno { get; set; }

        public string ipToken { get; set; }
        public string ipRequest { get; set; }
        public DateTime dateEnter { get; set; }

        public string headerUserIdentifier
        {
            get
            {
                string value = "{\"id\":\"0\",\"user_name\":\"" + this.account + "\",\"email\":\"\", \"remote_ip\":\"" + this.ipRequest + "\" }";

                byte[] byt = System.Text.Encoding.UTF8.GetBytes(value);
                // convert the byte array to a Base64 string
                value = Convert.ToBase64String(byt);

                return value;
            }
        }
    }
}
