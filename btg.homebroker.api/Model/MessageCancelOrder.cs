﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class MessageCancelOrder
    {
        public string orderid { get; set; }

        public string origclordid { get; set; }

        public string market { get; set; }

        public string quote { get; set; }

        public string qtd { get; set; }

        public string side { get; set; }

        public string username { get; set; }

        public string enteringtrader { get; set; }

        public string account { get; set; }   

    }
}
