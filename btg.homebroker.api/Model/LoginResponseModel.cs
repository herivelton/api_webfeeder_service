﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class LoginResponseModel
    {
        public string Msg { get; set; }
        public string Status { get; set; }
    }

    public class InternalLoginResponseModel
    {
        public string authenticationReqId { get; set; }
        public string isAuthenticated { get; set; }
        public string text { get; set; }
    }

    public class AuthenticationResponseModel
    {
        public bool success { get; set; }
        public string token { get; set; }
    }

    public class TokenResquestModel
    {
        public string Token { get; set; }
    }

    public class TokenResponseModel
    {
        public TokenResponseModel() {}
        public TokenResponseModel(string _url, string _issuer, string _audience, string _key)
        {
            issuer = _issuer;
            audience = _audience;
            key = _key;
        }

        public bool isDebug { get; set; }
        public string code { get; set; }
        public string codeMessage { get; set; }
        public string message { get; set; }
        public string token { get; set; }
        public string tokenWS { get; set; }
        public string tokenWK { get; set; }
        public string accountNumber { get; set; }
        public string issuer { get; set; }
        public string audience { get; set; }
        public string key { get; set; }
    }
}
