﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace btg.homebroker.api.Model
{
    public class MessageTrade
    {
        public string account { get; set; }

        public string origclordid { get; set; }

        public string orderid { get; set; }

        public string market { get; set; }

        public string price { get; set; }

        public string quote { get; set; }

        public int qtd { get; set; }

        public int loteDefault { get; set; }

        public string maxfloor { get; set; }

        public string type { get; set; }

        public string side { get; set; }

        public string username { get; set; }

        public string validityorder { get; set; }

        public string targettrigger { get; set; }

        public string targetlimit { get; set; }

        public string stoptrigger { get; set; }

        public string stoplimit { get; set; }

        public string movingstart { get; set; }

        public string initialchangetype { get; set; }

        public string initialchange { get; set; }

        public string pricepercentage { get; set; }
        
        public string orderstrategy { get; set; }

        public string timeinforce { get; set; }

        public string enteringtrader { get; set; }

        public string warrantyID { get; set; }

        public string descWarranty { get; set; }

        public string quantity { get; set; }
        public string valueWarranty { get; set; }
        public string operation { get; set; }

    }
}
