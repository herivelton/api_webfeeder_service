﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using btg.homebroker.api.Model;
using Microsoft.AspNetCore.Authorization;
using Serilog;
using System;

namespace btg.homebroker.api.Controllers
{
#if DEBUG || LEROSA
    [EnableCors("AllowCors"), Route("services/[controller]")]
    [Authorize]
    public class TreasureController : BaseController
    {

        #region Construtor

        //private readonly ILogger _logger;
        private static BLL.WebfeederServicesBLL _webFeederServiceInstance;

        public TreasureController(BLL.WebfeederServicesBLL webFeederServiceInstance) //, ILogger logger)
        {
            _webFeederServiceInstance = webFeederServiceInstance;
            _webFeederServiceInstance._logger = Log.Logger;
        }

         #endregion

        /// <summary>
        /// Consulta ofertas Tesouro Direto.
        /// </summary>
        /// <returns></returns>
        [HttpGet("getDirectTreasureOffers")]
        public async Task<IActionResult> GetDirectTreasureOffers(string query)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetDirectTreasureOffers(query, ipRequest));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

    }
#endif
}
