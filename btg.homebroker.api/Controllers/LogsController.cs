﻿using btg.homebroker.api.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using Microsoft.AspNetCore.Authorization;

namespace btg.homebroker.api.Controllers
{
    [EnableCors("AllowCors"), Route("services/[controller]")]
    [Authorize]
    public class LogsController : BaseController
    {
        #region Construtor

        private static BLL.WebfeederServicesBLL _webFeederServiceInstance;

        /// <summary>
        /// Log
        /// </summary>
        /// <param name="webFeederServiceInstance"></param>
        public LogsController(BLL.WebfeederServicesBLL webFeederServiceInstance)
        {
            _webFeederServiceInstance = webFeederServiceInstance;
            _webFeederServiceInstance._logger = Log.Logger;
        }

        #endregion

        /// <summary>
        /// Escrever log no serviço web
        /// </summary>
        /// <param name="logModel"></param>
        /// <returns></returns>
        [HttpPost("writeLog")]
        public async Task<IActionResult> writeLog([FromBody]LogModel logModel)
        {
            string ipRequest = this.GetIpRequest();

            LogMessage(logModel.message, ipRequest);
            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageInfo"></param>
        private void LogMessage(string messageParameter, string ipRequest)
        {
            string messageInfo = string.Format("[{0}] {1}", ipRequest, messageParameter);
            var info = new
            {
                IP = ipRequest,
                Message = messageInfo
            };

            Console.WriteLine(messageInfo);
            Log.Warning("Logs: {@Info}", info);

            Console.WriteLine(messageInfo);
            Log.Warning(messageInfo);
        }

    }
}
