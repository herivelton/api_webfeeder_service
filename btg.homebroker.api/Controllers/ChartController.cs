﻿using btg.homebroker.api.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace btg.homebroker.api.Controllers
{
	[EnableCors("AllowCors"), Route("services/[controller]")]
	[Authorize]
	public class ChartController : BaseController
	{
		private const string MARKET = "BMFBOVESPA";

		#region Construtor

		private IMemoryCache _cache;
		private static BLL.WebfeederServicesBLL _webFeederServiceInstance;

		public ChartController(BLL.WebfeederServicesBLL webFeederServiceInstance, IMemoryCache memoryCache)
		{
			_webFeederServiceInstance = webFeederServiceInstance;
			_webFeederServiceInstance._logger = Log.Logger;
			_cache = memoryCache;
		}

		#endregion

		#region time

		/// <summary>
		/// Time
		/// </summary>
		/// <returns></returns>
		[HttpGet("time")]
		public async Task<IActionResult> time()
		{
			DateTime foo = DateTime.Now;
			long unixTime = ((DateTimeOffset)foo).ToUnixTimeSeconds();

			return Ok(unixTime);
		}

		#endregion

		#region config

		/// <summary>
		/// Config
		/// </summary>
		/// <returns></returns>
		[HttpGet("config")]
		public async Task<IActionResult> config()
		{
			//supports_search: false,
			//supports_group_request: true,
			//supports_marks: false,
			//supports_timescale_marks: false
			//supported_resolutions: ["1", "5", "10", "15", "30", "60", "1D", "1W", "1M"],

			var config = "{" +
						 "\"supports_search\": true," +
						 "\"supports_group_request\": false," +
						 "\"supports_marks\": false," +
						 "\"supports_timescale_marks\": false," +
						 "\"supports_time\": true," +
						 "\"exchanges\": [" +
							 "{" +
							 "  \"value\":\"" + MARKET + "\"," +
							 "  \"name\":\"BM&F\"," +
							 "  \"desc\":\"BM&F Bovespa\"" +
							 "}" +
						 "]," +
						 "\"symbolsTypes\":[" +
							 "{" +
							 "  \"name\":\"All types\"," +
							 "  \"value\":\"\"" +
							 "}," +
							 "{" +
							 "  \"name\":\"Stock\"," +
							 "  \"value\":\"stock\"" +
							 "}," +
							 "{" +
							 "  \"name\":\"Futures\"," +
							 "  \"value\":\"futures\"" +
							 "}" +
						 "]," +
						 "\"supportedResolutions\": [\"1\", \"5\", \"15\", \"60\", \"1D\", \"1W\", \"1M\"]" +
						 "}";
			// "\"supportedResolutions\": [\"D\", \"2D\", \"3D\", \"W\", \"3W\", \"M\", \"6M\"]" +
			// ["1", "5", "15", "30", "60", "1D", "1W", "1M"]
			return Ok(config);
		}

		#endregion

		#region search

		/// <summary>
		/// search
		/// </summary>
		/// <param name="limit"></param>
		/// <param name="query"></param>
		/// <param name="type"></param>
		/// <param name="exchange"></param>
		/// <returns></returns>
		[HttpGet("search")]
		public async Task<IActionResult> search(string limit, string query, string type, string exchange)
		{
			try
			{
				string ipRequest = this.GetIpRequest();
				MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

				var retorno = "";
				if (string.IsNullOrEmpty(query) || string.IsNullOrWhiteSpace(query))
				{
					query = "";
				}
				else
				{
					query = query.ToLower();
				}

				if (query.Length > 0)
				{
					string keysEntry = "ChartSearch#" + query;
					// Look for cache key.
					if (!_cache.TryGetValue(keysEntry, out retorno))
					{
						retorno = await _webFeederServiceInstance.GetQuoteQuery(query, 10, "", "", ipRequest, tokenJWT);

						// Set cache options.
						var cacheEntryOptions = new MemoryCacheEntryOptions()
							// Keep in cache for this time, reset time if accessed.
							.SetSlidingExpiration(TimeSpan.FromHours(10));

						// Save data in cache.
						_cache.Set(keysEntry, retorno, cacheEntryOptions);
					}
					else
					{
						this.LogMessageCache("services/chart/search?symbol=", query, ipRequest);
					}
				}

				if (string.IsNullOrEmpty(retorno))
				{
					retorno = "[]";
				}

				var quoteList = JsonConvert.DeserializeObject<List<QuoteQueryResponseModel>>(retorno);

				var retornoList = (from item in quoteList
								   select
								   new
								   {
									   description = item.company,
									   exchange = MARKET,
									   full_name = item.symbol,
									   symbol = item.symbol,
									   type = "stock"
								   })
								   .Take(30);
				//[{
				//description: "VERDESCAMPOS"
				//exchange: "BMFBOVESPA"
				//full_name: "AGVE5L"
				//symbol: "AGVE5L"
				//type: "stock"
				//}]

				return Ok(JsonConvert.SerializeObject(retornoList));
			}
			catch (Exception ex)
			{
				return this.GetStatusCode(ex);
			}
		}

		#endregion

		#region symbols

		/// <summary>
		/// symbols
		/// </summary>
		/// <returns></returns>
		[HttpGet("symbols")]
		public async Task<IActionResult> symbols(string symbol)
		{
			try
			{
				//stock
				//index
				//futures
				// 1,2,47,48,52,119
				var quote = symbol.ToUpper();
				var ret = "{\"name\":\"" + quote + "\"," +
						  "\"exchange-traded\":\"" + MARKET + "\"," +
						  "\"exchange-listed\":\"" + MARKET + "\"," +
						  "\"timezone\":\"America/Sao_Paulo\"," +
						  "\"minmov\":1," +
						  "\"minmov2\":0," +
						  "\"pointvalue\":1," +
						  "\"has_intraday\":true," +
						  "\"has_no_volume\":true," +
						  "\"type\":\"stock\"," +
						  "\"supportedResolutions\": [\"1\", \"5\", \"10\", \"15\", \"30\", \"60\", \"1D\", \"1W\", \"1M\"]," +
						  "\"pricescale\":100," +
						  "\"ticker\":\"" + quote + "\"}";

				// S-semanal, D-diário, M-mensal
				// Minutos passar valores (1, 5, 10, 15, 30, 60);

				return Ok(ret);
			}
			catch (Exception ex)
			{
				return this.GetStatusCode(ex);
			}
		}

		#endregion

		#region history

		/// <summary>
		/// history
		/// </summary>
		/// <param name="symbol">teste</param>
		/// <param name="resolution"></param>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <returns></returns>
		[HttpGet("history")]
		public async Task<IActionResult> history(string symbol, string resolution, string from, string to)
		{
			try
			{
				symbol = symbol.ToLower();
				string keysEntry = DateTime.Now.ToString("HHmmss");

				string ipRequest = this.GetIpRequest();
				MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                string retorno;
                string keysEntryCache = "getHistoryChart#Key#" + symbol + "#Resolution#" + resolution + "#from#" + from + "#to#" + to;
                if (!_cache.TryGetValue(keysEntryCache, out retorno))
                {
                    retorno = await _webFeederServiceInstance.GetCandleChart(symbol, resolution, keysEntry, from, to, ipRequest, tokenJWT);

                    // Set cache options.
                    var cacheEntryOptions = new MemoryCacheEntryOptions()
                        // Keep in cache for this time, reset time if accessed.
                        .SetSlidingExpiration(TimeSpan.FromHours(10));

                    // Save data in cache.
                    _cache.Set(keysEntryCache, retorno, cacheEntryOptions);
                }
                else
                {
                    if (string.IsNullOrEmpty(retorno))
                    {
                        _cache.Remove(keysEntryCache);
                        retorno = await _webFeederServiceInstance.GetCandleChart(symbol, resolution, keysEntry, from, to, ipRequest, tokenJWT);

                        // Set cache options.
                        var cacheEntryOptions = new MemoryCacheEntryOptions()
                            // Keep in cache for this time, reset time if accessed.
                            .SetSlidingExpiration(TimeSpan.FromHours(10));

                        // Save data in cache.
                        _cache.Set(keysEntryCache, retorno, cacheEntryOptions);
                    }
                    this.LogMessageCache("services/quotes/history", "", ipRequest);
                }

				if (string.IsNullOrEmpty(retorno))
				{
					retorno = "[]";
				}

				var candleChartList = JsonConvert.DeserializeObject<CandleChartResponse>(retorno);

				CandleTradingViewResponse retornoCandle = ConvertFromatToTradingView(candleChartList);

				//return Ok(retorno);
				return Ok(JsonConvert.SerializeObject(retornoCandle));

			}
			catch (Exception ex)
			{
				return this.GetStatusCode(ex);
			}
		}

		/// <summary>
		/// s: status code. Expected values: ok | error | no_data
		/// errmsg: error message.Should be present just if s = 'error'
		/// t: bar time. unix timestamp (UTC)
		/// c: close price
		/// o: open price (optional)
		/// h: high price (optional)
		/// l: low price (optional)
		/// v: volume (optional)
		/// </summary>
		/// <param name="candleChartList"></param>
		/// <returns></returns>
		private CandleTradingViewResponse ConvertFromatToTradingView(CandleChartResponse candleChartList)
		{
			CandleTradingViewResponse retornoCandle = new CandleTradingViewResponse();

			foreach (var item in candleChartList.candles)
			{
				var dateUnix = ConvertToTimestapUnix(item.date);

				retornoCandle.t.Add(dateUnix);
				retornoCandle.c.Add(item.price);
				retornoCandle.o.Add(item.open);
				retornoCandle.h.Add(item.high);
				retornoCandle.l.Add(item.low);
				retornoCandle.v.Add(item.fVol);
			}

			// ok | error | no_data
			if (candleChartList.candles.Count() > 0)
			{
				retornoCandle.s = "ok";
			}
			else
			{
				retornoCandle.s = "no_data";
			}

			return retornoCandle;
		}

		private long ConvertToTimestapUnix(long date)
		{
			var foo = DateTime.ParseExact(date.ToString(),
						"yyyyMMddHHmm",
						System.Globalization.CultureInfo.InvariantCulture);

			//DateTime foo = DateTime.Now;
			long unixTime = ((DateTimeOffset)foo).ToUnixTimeSeconds();

			return unixTime;
		}

		#endregion

	}
}
