﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Serilog;
using btg.homebroker.api.Model;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;

namespace btg.homebroker.api.Controllers
{
    [EnableCors("AllowCors"), Route("services/[controller]")]
    public class AuthenticateController : BaseController
    {
        #region "Construtor"

        private static string ClaimAccount = "account";
        private static string ClaimIP = "IP";
        //private readonly ILogger _logger;
        private IMemoryCache _cache;
        private static BLL.BTGBLL _tokenInstance;
        private static BLL.WebfeederServicesBLL _webFeederServiceInstance;

        public AuthenticateController(BLL.BTGBLL tokenInstance, BLL.WebfeederServicesBLL webFeederServiceInstance, IMemoryCache memoryCache) //, ILogger logger)
        {
            _webFeederServiceInstance = webFeederServiceInstance;
            _tokenInstance = tokenInstance;
            _tokenInstance._logger = Log.Logger;
            _cache = memoryCache;
            //_logger = logger;
        }

        #endregion

        #region "ValideToken"

        /// <summary>
        /// Validar item atual
        /// </summary>
        /// <param name="tkn"></param>
        /// <returns></returns>
        [HttpPost("ValideToken")]
        public async Task<IActionResult> ValideToken([FromBody]MessageValideToken tkn)
        {
            try
            {
                if (!string.IsNullOrEmpty(tkn.account) &&
                    !string.IsNullOrEmpty(tkn.tokenA) &&
                    !string.IsNullOrEmpty(tkn.tokenW))
                {

                    // Usuário de DSV, retornar sem precisar logar no OMS
                    string guidUser = Guid.NewGuid().ToString();
                    string accountNumber = tkn.account;
                    string ipRequest = this.GetIpRequest();
                    var tokenAPI = tkn.tokenA;
                    string tokenHeader = Convert.ToString(Request.Headers["Authorization"]);
                    tokenHeader = tokenHeader.Replace("Bearer ", "");

                    if (tokenAPI == tokenHeader)
                    {
                        MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                        guidUser = this.GetCacheUserToken(_cache, tokenJWT.account);

                        // TokenInterno == TokenHeader
                        if (tokenJWT.tokenInterno == guidUser
                         && tokenJWT.account == accountNumber)
                        {
                            return Ok(new
                            {
                                code = "200",
                                codeMessage = "",
                                accountNumber = tkn.account,
                                guidUser = guidUser,
                                tokenWS = "",
                                token = tokenAPI,
                                expiration = ""
                            });
                        }
                        else
                        {
                            return Ok(new
                            {
                                code = "400",
                                codeMessage = "BAD_RESQUEST",
                                message = "INVALID_GUID",
                                accountNumber = "",
                                guidUser = "",
                                tokenWS = "",
                                token = "",
                                expiration = ""
                            });
                        }
                    }
                }

                return Ok(new
                {
                    code = "500",
                    codeMessage = "INTERNAL_SERVER_ERROR",
                    message = "Token não autorizado.",
                    accountNumber = "",
                    guidUser = "",
                    tokenWS = "",
                    token = "",
                    expiration = ""
                });
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// Listagem de usuários
        /// </summary>
        /// <returns></returns>
        [HttpPost("GetUser")]
        public async Task<IActionResult> GetUserOnline()
        {
            try
            {
                Dictionary<string, MessageUserCache> dictUser = this.GetCacheUser(_cache);
                string sReturn = string.Empty;
                string sVirgula = string.Empty;

                foreach (var item in dictUser)
                {
                    MessageUserCache userCache = item.Value;
                    sReturn += sVirgula +
                        "{ \"a\": \"" + item.Key +
                        "\", \"t\": \"" + userCache.guid +
                        "\", \"i\": \"" + userCache.ipRequest +
                        "\", \"d\": \"" + userCache.dateIn.ToString() +
                        "\"}";
                    sVirgula = ",";
                }

                sReturn = "[" + sReturn + "]";

                return Ok(sReturn);
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        #endregion

        #region "LoginBTG"

#if  DEBUG || BTG

        /// <summary>
        /// Realiza o envio do token
        /// </summary>
        /// <param name="tkn">Token para autorização</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("LoginB")]
        public async Task<IActionResult> LoginBTG([FromBody]TokenResquestModel tkn)
        {
            try
            {
                if (tkn == null || string.IsNullOrEmpty(tkn.Token))
                {
                    //Response NOK:
                    // return BadRequest();
                    return Ok("{ \"code\": \"400\", \"codeMessage\": \"BAD_REQUEST\", \"message\": \"O parametro é obrigatório\"}");
                }

                string ipRequest = this.GetIpRequest();

                if (_webFeederServiceInstance.GetTokenRefresh())
                {
                    LoginResponseModel loginResponse = await _webFeederServiceInstance.RefreshTokenAsync(ipRequest);
                }

                TokenResponseModel tokenResponse;
                try
                {
                    tokenResponse = await _tokenInstance.PostLogin_BTG(tkn.Token, ipRequest);

                    if (tokenResponse.isDebug)
                    {
                        // Usuário de DSV, retornar sem precisar logar no OMS
                        string guidUser = Guid.NewGuid().ToString();
                        string accountNumber = tokenResponse.accountNumber;

                        //var response = await _webFeederServiceInstance.LoginHomeBroker(accountNumber, tkn.Token, ipRequest);
                        //_webFeederServiceInstance.SetTokenRefresh();

                        var token = await GetJwtSecurityToken(guidUser, accountNumber, ipRequest, tokenResponse);
                        // Setar cache user/account
                        this.SetCacheUser(_cache, accountNumber, guidUser, ipRequest, true);

                        MessageTokenJWT tokenJWT = new MessageTokenJWT();
                        tokenJWT.ipRequest = ipRequest;
                        tokenJWT.ipToken = ipRequest;
                        tokenJWT.account = tokenResponse.accountNumber;

                        var responseTokenPrincipal = await _webFeederServiceInstance.AuthenticationToken(ipRequest, tokenJWT);
                        var responseTokenWorksheet = await _webFeederServiceInstance.AuthenticationToken(ipRequest, tokenJWT);

                        if (!(responseTokenPrincipal.success && responseTokenWorksheet.success))
                        {
                            return Ok(new
                            {
                                code = "400",
                                codeMessage = "BAD_REQUEST",
                                message = "Erro em obter token.",
                                accountNumber = "",
                                tokenWS = "",
                                tokenWK = "",
                                token = "",
                                expiration = ""
                            });
                        }

                        tokenResponse.tokenWS = responseTokenPrincipal.token;
                        tokenResponse.tokenWK = responseTokenWorksheet.token;

                        return Ok(new
                        {
                            code = tokenResponse.code,
                            codeMessage = tokenResponse.codeMessage,
                            accountNumber = tokenResponse.accountNumber,
                            guidUser = guidUser,
                            tokenWS = tokenResponse.tokenWS,
                            tokenWK = tokenResponse.tokenWK,
                            token = new JwtSecurityTokenHandler().WriteToken(token),
                            expiration = token.ValidTo
                        });
                    }
                }
                catch (Exception ex)
                {
                    return Ok(new
                    {
                        code = "500",
                        codeMessage = "INTERNAL_SERVER_ERROR",
                        message = "Token não autorizado. " + ex.Message,
                        accountNumber = "",
                        tokenWS = "",
                        tokenWK = "",
                        token = "",
                        expiration = ""
                    });
                }


                if (!string.IsNullOrEmpty(tokenResponse.accountNumber))
                {
                    MessageTokenJWT tokenJWT = new MessageTokenJWT();
                    tokenJWT.ipRequest = ipRequest;
                    tokenJWT.ipToken = ipRequest;
                    tokenJWT.account = tokenResponse.accountNumber;

                    string accountNumber = tokenResponse.accountNumber;
                    var response = await _webFeederServiceInstance.LoginHomeBroker(accountNumber, tkn.Token, ipRequest, tokenJWT);
                    _webFeederServiceInstance.SetTokenRefresh();

                    if (response.isAuthenticated == "N")
                    {
                        return Ok(new
                        {
                            code = "401",
                            codeMessage = "UNAUTHORIZED",
                            message = response.text,
                            accountNumber = response.authenticationReqId,
                            tokenWS = "",
                            tokenWK = "",
                            token = "",
                            expiration = ""
                        });
                    }

                    var responseTokenPrincipal = await _webFeederServiceInstance.AuthenticationToken(ipRequest, tokenJWT);
                    var responseTokenWorksheet = await _webFeederServiceInstance.AuthenticationToken(ipRequest, tokenJWT);

                    if (!(responseTokenPrincipal.success && responseTokenWorksheet.success))
                    {
                        return Ok(new
                        {
                            code = "400",
                            codeMessage = "BAD_REQUEST",
                            message = "Erro em obter token.",
                            accountNumber = "",
                            tokenWS = "",
                            tokenWK = "",
                            token = "",
                            expiration = ""
                        });
                    }

                    tokenResponse.tokenWS = responseTokenPrincipal.token;
                    tokenResponse.tokenWK = responseTokenWorksheet.token;

                    string guidUser = Guid.NewGuid().ToString();
                    var token = await GetJwtSecurityToken(guidUser, accountNumber, ipRequest, tokenResponse);


                    string keysEntry = "jwt#" + accountNumber;
                    string retorno = "";
                    // Look for cache key.
                    if (!_cache.TryGetValue(keysEntry, out retorno))
                    {
                        // Set cache options.
                        var cacheEntryOptions = new MemoryCacheEntryOptions()
                            // Keep in cache for this time, reset time if accessed.
                            .SetSlidingExpiration(TimeSpan.FromHours(12));

                        // Save data in cache.
                        _cache.Set(keysEntry, guidUser, cacheEntryOptions);
                    }
                    else
                    {
                        var guidCache = _cache.Get(keysEntry);
                    }

                    // Setar cache user/account
                    this.SetCacheUser(_cache, accountNumber, guidUser, ipRequest, true);

                    // return Ok(tokenResponse);
                    return Ok(new
                    {
                        code = tokenResponse.code,
                        codeMessage = tokenResponse.codeMessage,
                        accountNumber = tokenResponse.accountNumber,
                        tokenWS = tokenResponse.tokenWS,
                        tokenWK = tokenResponse.tokenWK,
                        guidUser = guidUser,
                        token = new JwtSecurityTokenHandler().WriteToken(token),
                        expiration = token.ValidTo
                    });
                }
                else
                {
                    // return Ok(tokenResponse);
                    return Ok(new
                    {
                        code = tokenResponse.code,
                        codeMessage = tokenResponse.codeMessage,
                        message = tokenResponse.message,
                        accountNumber = "",
                        tokenWS = "",
                        tokenWK = "",
                        token = "",
                        expiration = ""
                    });

                }
                //return Ok(await _tokenInstance.PostLogin(tkn.Token));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

#endif

        #endregion

        #region "LoginVexter - PostLogin"

#if DEBUG || VEXTER

        /// <summary>
        /// Realiza o envio do token para login no portal
        /// </summary>
        /// <param name="tkn">Token para autorização</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("PostLogin")]
        public async Task<IActionResult> PostLogin([FromBody]TokenResquestModel tkn)
        {
            try
            {
                if (tkn == null || string.IsNullOrEmpty(tkn.Token))
                {        
                    //Response NOK:
                    // return BadRequest();
                    return Ok("{ \"code\": \"400\", \"codeMessage\": \"BAD_REQUEST\", \"message\": \"O parametro é obrigatório\"}");
                }

                string ipRequest = this.GetIpRequest();

                TokenResponseModel tokenResponse;
                tokenResponse = await _tokenInstance.validAccountNumber(tkn.Token, ipRequest);

                if (tokenResponse.isDebug)
                {
                    // Usuário de DSV, retornar sem precisar logar no OMS
                    string guidUser = Guid.NewGuid().ToString();
                    string accountNumber = tokenResponse.accountNumber;
                    var token = await GetJwtSecurityToken(guidUser, accountNumber, ipRequest, tokenResponse);
                    // Setar cache user/account
                    this.SetCacheUser(_cache, accountNumber, guidUser, ipRequest, true);

                    return Ok(new
                    {
                        code = tokenResponse.code,
                        codeMessage = tokenResponse.codeMessage,
                        accountNumber = tokenResponse.accountNumber,
                        tokenWS = tokenResponse.tokenWS,
                        token = new JwtSecurityTokenHandler().WriteToken(token),
                        expiration = token.ValidTo
                    });
                }

                return Ok(new
                {
                    code = "500",
                    codeMessage = "INTERNAL_SERVER_ERROR",
                    message = "Token não autorizado.",
                    accountNumber = "",
                    tokenWS = "",
                    token = "",
                    expiration = ""
                });


            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

#endif

        private async Task<JwtSecurityToken> GetJwtSecurityToken(string guidUser, string accountNumber, string ipRequest, TokenResponseModel tokenResponse)
        {
            return new JwtSecurityToken(
                issuer: tokenResponse.issuer,
                audience: tokenResponse.audience,
                claims: GetTokenClaims(guidUser, accountNumber, ipRequest),
                expires: DateTime.UtcNow.AddMinutes(720),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenResponse.key)), SecurityAlgorithms.HmacSha256)
            );
        }
        private static IEnumerable<Claim> GetTokenClaims(string guidUser, string accountNumber, string ipRequest)
        {
            return new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, guidUser),
                new Claim(ClaimAccount, accountNumber),
                new Claim(ClaimIP, ipRequest)
            };
        }

        #endregion


#if DEBUG || FAST

        #region "LoginHomeBroker"

        /// <summary>
        /// Realiza Login no FastTradeWeb
        /// </summary>
        /// <param name="userHomeBroker"></param>
        /// <returns></returns>
        // [Authorize]
        [AllowAnonymous]
        [HttpPost("LoginO")]
        public async Task<IActionResult> LoginO([FromBody]MessageUserHomeBroker userHomeBroker)
        {
            try
            {
                string ipRequest = this.GetIpRequest();

                MessageTokenJWT tokenJWT = new MessageTokenJWT();
                tokenJWT.ipRequest = ipRequest;
                tokenJWT.ipToken = ipRequest;
                tokenJWT.account = userHomeBroker.username;

                string accountNumber = userHomeBroker.username;
                string accountPass = userHomeBroker.password;
                var response = await _webFeederServiceInstance.LoginHomeBroker(accountNumber, accountPass, ipRequest, tokenJWT);
                _webFeederServiceInstance.SetTokenRefresh();

                if (response.isAuthenticated == "N")
                {
                    return Ok(new
                    {
                        code = "401",
                        codeMessage = "UNAUTHORIZED",
                        message = response.text,
                        accountNumber = response.authenticationReqId,
                        tokenWS = "",
                        token = "",
                        expiration = ""
                    });
                }

                var responseToken = await _webFeederServiceInstance.AuthenticationToken(ipRequest, tokenJWT);

                if (!responseToken.success)
                {
                    return Ok(new
                    {
                        code = "500",
                        codeMessage = "INTERNAL_ERROR",
                        message = "Erro em conectar com o webFeeder.",
                        accountNumber = response.authenticationReqId,
                        tokenWS = "",
                        token = "",
                        expiration = ""
                    });
                }

                TokenResponseModel tokenResponse = await _tokenInstance.PostLogin_FastTradeWeb(accountNumber, ipRequest);

                string guidUser = Guid.NewGuid().ToString();
                var token = await GetJwtSecurityToken(guidUser, accountNumber, ipRequest, tokenResponse);


                string keysEntry = "jwt#" + accountNumber;
                string retorno = "";
                // Look for cache key.
                if (!_cache.TryGetValue(keysEntry, out retorno))
                {
                    // Set cache options.
                    var cacheEntryOptions = new MemoryCacheEntryOptions()
                        // Keep in cache for this time, reset time if accessed.
                        .SetSlidingExpiration(TimeSpan.FromHours(12));

                    // Save data in cache.
                    _cache.Set(keysEntry, guidUser, cacheEntryOptions);
                }
                else
                {
                    var guidCache = _cache.Get(keysEntry);
                }

                // Setar cache user/account
                this.SetCacheUser(_cache, accountNumber, guidUser, ipRequest, true);

                // return Ok(tokenResponse);
                return Ok(new
                {
                    code = tokenResponse.code,
                    codeMessage = tokenResponse.codeMessage,
                    accountNumber = tokenResponse.accountNumber,
                    tokenWS = responseToken.token,
                    guidUser = guidUser,
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });

            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        #endregion

        #region "LoginHomeBroker"

        /// <summary>
        /// Realiza Login
        /// </summary>
        /// <param name="messageUserHomeBroker"></param>
        /// <returns></returns>
        // [Authorize]
        [AllowAnonymous]
        [HttpPost("LoginHomeBroker")]
        public async Task<IActionResult> LoginHomeBroker([FromBody]MessageUserHomeBroker messageUserHomeBroker)
        {
            string ipRequest = this.GetIpRequest();
            MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

            return Ok(await _webFeederServiceInstance.LoginHomeBroker(
                                          messageUserHomeBroker.username,
                                          messageUserHomeBroker.password,
                                          ipRequest,
                                          tokenJWT
                                          ));
        }

        #endregion

        #region "LogoutHomeBroker"

        /// <summary>
        /// Realiza o Logout
        /// </summary>
        /// <param name="messageUserHomeBroker"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("LogoutHomeBroker")]
        public async Task<IActionResult> LogoutHomeBroker([FromBody]MessageUserHomeBroker messageUserHomeBroker)
        {
            string ipRequest = this.GetIpRequest();
            MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

            return Ok(await _webFeederServiceInstance.LogoutHomeBroker(
                                        messageUserHomeBroker.username,
                                        ipRequest,
                                        tokenJWT
                                        ));
        }

        #endregion

        #region "ChangePasswordHomeBroker"

        /// <summary>
        /// Altera a senha do usuario
        /// </summary>
        /// <param name="messageUserHomeBroker"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("ChangePasswordHomeBroker")]
        public async Task<IActionResult> ChangePasswordHomeBroker([FromBody]MessageUserHomeBroker messageUserHomeBroker)
        {
            string ipRequest = this.GetIpRequest();
            MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

            return Ok(await _webFeederServiceInstance.ChangePasswordHomeBroker(
                                        messageUserHomeBroker.username,
                                        messageUserHomeBroker.password,
                                        messageUserHomeBroker.newPassword,
                                        ipRequest,
                                        tokenJWT
                                        ));
        }

        #endregion

        #region "ForgotPasswordHomeBroker"

        /// <summary>
        /// Recupera senha
        /// </summary>
        /// <param name="messageUserHomeBroker"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("ForgotPasswordHomeBroker")]
        public async Task<IActionResult> ForgotPasswordHomeBroker([FromBody]MessageUserHomeBroker messageUserHomeBroker)
        {
            string ipRequest = this.GetIpRequest();
            MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

            return Ok(await _webFeederServiceInstance.ForgotPasswordHomeBroker(messageUserHomeBroker.username,
                                                                               messageUserHomeBroker.clientDocument,
                                                                               ipRequest,
                                                                               tokenJWT
                                                                               ));
        }

        #endregion

#endif
    }
}
