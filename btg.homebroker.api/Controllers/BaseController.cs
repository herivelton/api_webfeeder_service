﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Serilog;
using btg.homebroker.api.Model;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Caching.Memory;

namespace btg.homebroker.api.Controllers
{
    public class BaseController : Controller
    {
        #region "GetIpRequest"

        /// <summary>
        /// Obter o IP remoto da request
        /// </summary>
        /// <returns></returns>
        protected string GetIpRequest()
        {
            var ipAddress = Request?.Headers["X-Real-IP"];
            //var ipAddress = Request?.Headers["X-Forwarded-For"];

            if (string.IsNullOrEmpty(ipAddress))
            {
                ipAddress = Request?.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            }
            return ipAddress;
        }

        #endregion

        #region "GetTokenJWT"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipRequest"></param>
        /// <returns></returns>
        protected MessageTokenJWT GetTokenJWT(string ipRequest)
        {
            MessageTokenJWT tokenJWT = new MessageTokenJWT();
            string tokenHeader = Convert.ToString(Request.Headers["Authorization"]);
            if (!string.IsNullOrEmpty(tokenHeader))
            {
                tokenHeader = tokenHeader.Replace("Bearer ", "");

                JwtSecurityToken jwtSecurityToken = new JwtSecurityTokenHandler().ReadJwtToken(tokenHeader);

                tokenJWT.account = Convert.ToString(jwtSecurityToken.Payload["account"]);
                tokenJWT.tokenInterno = Convert.ToString(jwtSecurityToken.Payload["jti"]);
                tokenJWT.ipToken = Convert.ToString(jwtSecurityToken.Payload["IP"]);
                tokenJWT.ipRequest = ipRequest;
            }
            return tokenJWT;
        }

        #endregion

        #region "GetStatusCode"

        /// <summary>
        /// Tratar status code de retorno
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        protected StatusCodeResult GetStatusCode(Exception ex)
        {
            if (ex.Message.IndexOf("StatusCode=401") == 0)
            {
                return StatusCode(401);
            }
            else if (ex.Message.IndexOf("StatusCode=403") == 0)
            {
                return StatusCode(403);
            }
            else if (ex.Message.IndexOf("StatusCode=404") == 0)
            {
                return StatusCode(404);
            }
            else if (ex.Message.IndexOf("StatusCode=408") == 0)
            {
                return StatusCode(408);
            }
            return StatusCode((int)HttpStatusCode.InternalServerError);
        }

        #endregion

        #region "LogMessageCache"

        /// <summary>
        /// Gravar mensagem no log - cache
        /// </summary>
        /// <param name="_Method"></param>
        /// <param name="_Resource"></param>
        /// <param name="_ipRequest"></param>
        protected void LogMessageCache(string _Method, string _Resource, string _ipRequest)
        {
            //var Variables = Environment.GetEnvironmentVariables();
            // var machine = Environment.GetEnvironmentVariable("COMPUTERNAME");
            string messageInfo = string.Format("[{2}] CACHE {0}{1}", _Method, _Resource, _ipRequest);
            var info = new
            {
                IP = _ipRequest,
                Method = _Method,
                HashCode = "Cache",
                Resource = _Resource
            };

            Console.WriteLine(messageInfo);
            Log.Warning("Request: {@Info}", info);
        }

        #endregion


        #region "CacheUser"

        //private string KEYS_USER_GUID = "jwt#userGuid";
        private string KEYS_USER_ONLINE = "jwt#userOnline";

        /// <summary>
        /// Gravar cache do usuario para evitar dupla autenticação
        /// </summary>
        /// <param name="_memoryCache"></param>
        /// <param name="account"></param>
        /// <param name="guidUser"></param>
        /// <param name="ipRequest"></param>
        /// <param name="actionLogin"></param>
        protected void SetCacheUser(IMemoryCache _memoryCache, string account, string guidUser, string ipRequest, bool actionLogin)
        {
            // Set cache options.
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                // Keep in cache for this time, reset time if accessed.
                .SetSlidingExpiration(TimeSpan.FromHours(12));

            // Dictionary<string, string> retornoGuid = new Dictionary<string, string>();
            Dictionary<string, MessageUserCache> retornoUserOnline = new Dictionary<string, MessageUserCache>();
            // Look for cache key.
            _memoryCache.TryGetValue(KEYS_USER_ONLINE, out retornoUserOnline);

            if (retornoUserOnline == null)
            {
                retornoUserOnline = new Dictionary<string, MessageUserCache>();
            }

            MessageUserCache value = new MessageUserCache();
            value.account = account;
            value.guid = guidUser;
            value.dateIn = DateTime.Now;
            value.ipRequest = ipRequest;

            if (retornoUserOnline.ContainsKey(account))
            {
                retornoUserOnline.Remove(account);
                retornoUserOnline.Add(account, value);
            }
            else
            {
                retornoUserOnline.Add(account, value);
            }

            // Save data in cache.
            _memoryCache.Set(KEYS_USER_ONLINE, retornoUserOnline, cacheEntryOptions);
        }
        /// <summary>
        /// Obter cache do usuario para evitar dupla autenticação
        /// </summary>
        /// <param name="_memoryCache"></param>
        /// <returns></returns>
        protected Dictionary<string, MessageUserCache> GetCacheUser(IMemoryCache _memoryCache)
        {
            Dictionary<string, MessageUserCache> retorno = new Dictionary<string, MessageUserCache>();
            // Look for cache key.
            _memoryCache.TryGetValue(KEYS_USER_ONLINE, out retorno);

            if (retorno == null)
            {
                retorno = new Dictionary<string, MessageUserCache>();
            }

            return retorno;
        }
        /// <summary>
        /// Obter cache do usuario para evitar dupla autenticação
        /// </summary>
        /// <param name="_memoryCache"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        protected string GetCacheUserToken(IMemoryCache _memoryCache, string account)
        {
            Dictionary<string, MessageUserCache> retorno = new Dictionary<string, MessageUserCache>();
            // Look for cache key.
            _memoryCache.TryGetValue(KEYS_USER_ONLINE, out retorno);

            if (retorno == null)
            {
                retorno = new Dictionary<string, MessageUserCache>();
            }

            MessageUserCache value = new MessageUserCache();
            if (retorno.TryGetValue(account, out value))
            {
                return value.guid;
            }
            return string.Empty;
        }

        #endregion

    }
}
