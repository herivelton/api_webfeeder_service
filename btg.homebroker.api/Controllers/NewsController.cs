﻿using btg.homebroker.api.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using Microsoft.AspNetCore.Authorization;

namespace btg.homebroker.api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [EnableCors("AllowCors"), Route("services/[controller]")]
    [Authorize]
    public class NewsController : BaseController
    {

        #region Construtor

        //private readonly ILogger _logger;
        private static BLL.WebfeederServicesBLL _webFeederServiceInstance;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="webFeederServiceInstance"></param>
        public NewsController(BLL.WebfeederServicesBLL webFeederServiceInstance) //, ILogger logger)
        {
            _webFeederServiceInstance = webFeederServiceInstance;
            _webFeederServiceInstance._logger = Log.Logger;
            //_logger = logger;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="initialTime"></param>
        /// <param name="finalTime"></param>
        /// <returns></returns>
        [HttpGet("newsByDate")]
        public async Task<IActionResult> newsByDate(string initialTime, string finalTime)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);
                
                //return Ok(await _webFeederServiceInstance.GetNewsByDate(query, ipRequest, tokenJWT));
                return Ok(await _webFeederServiceInstance.GetNewsByDate(initialTime, finalTime, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="agency"></param>
        /// <returns></returns>
        [HttpGet("newsSearchByDate")]
        public async Task<IActionResult> GetNewsSearchByDate(string text, string agency)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                //return Ok(await _webFeederServiceInstance.GetNewsByDate(query, ipRequest, tokenJWT));
                return Ok(await _webFeederServiceInstance.GetNewsSearchByDate(text, agency, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="initialTime"></param>
        /// <param name="finalTime"></param>
        /// <returns></returns>
        [HttpGet("newsByDatePagination")]
        public async Task<IActionResult> newsByDatePagination(string initialTime, string finalTime)
        //public async Task<IActionResult> newsByDatePagination(string query)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetNewsByDatePagination(initialTime, finalTime, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="initialTime"></param>
        /// <param name="finalTime"></param>
        /// <returns></returns>
        [HttpGet("newsRelevantFacts")]
        public async Task<IActionResult> GetNewsRelevantFacts(string initialTime, string finalTime)
        //public async Task<IActionResult> GetNewsRelevantFacts(string query)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetNewsRelevantFacts(initialTime, finalTime, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

    }
}
