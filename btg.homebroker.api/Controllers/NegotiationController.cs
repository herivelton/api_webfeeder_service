﻿using btg.homebroker.api.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Serilog;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace btg.homebroker.api.Controllers
{
	[EnableCors("AllowCors"), Route("services/[controller]")]
	[Authorize]
	public class NegotiationController : BaseController
	{

		#region Construtor

		//private readonly ILogger _logger;
		private static BLL.WebfeederServicesBLL _webFeederServiceInstance;

		public NegotiationController(BLL.WebfeederServicesBLL webFeederServiceInstance) //, ILogger logger)
		{
			_webFeederServiceInstance = webFeederServiceInstance;
			_webFeederServiceInstance._logger = Log.Logger;
			//_logger = logger;
		}

		#endregion

		#region GetHistoryOrder

		/// <summary>
		///
		/// </summary>
		/// <param name="messageHistoryOrder"></param>
		/// <returns></returns>
		[HttpPost("GetHistoryOrder")]
		public async Task<IActionResult> GetHistoryOrder([FromBody]MessageHistoryOrderModel messageHistoryOrder)
		{
			try
			{
				//ToDo: Implementar validação de parametros
				//if (string.IsNullOrEmpty(messageHistoryOrder))
				//{
				//    return BadRequest("O parametro account é obrigatório.");
				//}

                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetHistoryOrder(
                                                   messageHistoryOrder.account,
                                                   messageHistoryOrder.market,
                                                   messageHistoryOrder.dataInicio,
                                                   messageHistoryOrder.dataFim,
                                                   messageHistoryOrder.symbol,
                                                   messageHistoryOrder.status,
                                                   messageHistoryOrder.direction,
                                                   messageHistoryOrder.orderID,
                                                   messageHistoryOrder.type,
                                                   messageHistoryOrder.statusGroup,
                                                   messageHistoryOrder.queryType,
                                                   ipRequest,
                                                   tokenJWT
                                                   ));

			}
			catch (Exception ex)
			{
				return this.GetStatusCode(ex);
			}
		}

		#endregion

		#region SendNewOrderSingle

        /// <summary>
        /// Realiza o envio de ordens via home broker
        /// </summary>
        /// <param name="messageTrade"></param>
        /// <returns></returns>
        [HttpPost("SendNewOrderSingle")]
        public async Task<IActionResult> SendNewOrderSingle(MessageTrade messageTrade)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.SendNewOrderSingle(
																			messageTrade.account,
																			messageTrade.market,
																			messageTrade.price,
																			messageTrade.quote,
																			messageTrade.qtd,
																			messageTrade.validityorder,
																			messageTrade.targettrigger,
																			messageTrade.targetlimit,
																			messageTrade.stoptrigger,
																			messageTrade.stoplimit,
																			messageTrade.type,
																			messageTrade.side,
																			messageTrade.username,
																			messageTrade.orderstrategy,
																			messageTrade.loteDefault,
																			ipRequest,
																			messageTrade.timeinforce,
                                                                            tokenJWT
																		));

			}
			catch (Exception ex)
			{
				return this.GetStatusCode(ex);
			}
		}

		#endregion

		#region SendNewOrderFractional (INATIVO)

        ///// <summary>
        ///// Realiza o envio de ordens via home broker
        ///// </summary>
        ///// <param name="messageTrade"></param>
        ///// <param name="account"></param>
        ///// <param name="market"></param>
        ///// <param name="price"></param>
        ///// <param name="quote"></param>
        ///// <param name="qtd"></param>
        ///// <param name="validityorder"></param>
        ///// <param name="targettrigger"></param>
        ///// <param name="targetlimit"></param>
        ///// <param name="stoptrigger"></param>
        ///// <param name="stoplimit"></param>
        ///// <param name="type"></param>
        ///// <param name="side"></param>
        ///// <param name="username"></param>
        ///// <returns></returns>
        //[HttpPost("SendNewOrderFractional")]
        ////[AuthorizeRoles(Constants.Policy.CLIENT)]
        //public async Task<IActionResult> SendNewOrderFractional([FromBody]MessageTrade messageTrade)
        //{
        //    try
        //    {
        //        string ipRequest = this.GetIpRequest();

		//        return Ok(await _webFeederServiceInstance.SendNewOrderSingle(
		//                                               messageTrade.account,
		//                                               messageTrade.market,
		//                                               messageTrade.price,
		//                                               messageTrade.quote,
		//                                               messageTrade.qtd,
		//                                               messageTrade.validityorder,
		//                                               messageTrade.targettrigger,
		//                                               messageTrade.targetlimit,
		//                                               messageTrade.stoptrigger,
		//                                               messageTrade.stoplimit,
		//                                               messageTrade.type,
		//                                               messageTrade.side,
		//                                               messageTrade.username,
		//                                               messageTrade.orderstrategy,
		//                                               messageTrade.loteDefault,
		//                                               ipRequest,
		//                                               messageTrade.timeinforce
		//                                             ));

		//    }
		//    catch (Exception ex)
		//    {
		//        return this.GetStatusCode(ex);
		//    }
		//}

		#endregion

		#region SendNewOrder

        /// <summary>
        /// Realiza o envio de ordens via home broker, calculando se há quantidade fracionada
        /// </summary>
        /// <param name="messageTrade"></param>
        /// <returns></returns>
        [HttpPost("SendNewOrder")]
        public async Task<IActionResult> SendNewOrder([FromBody]MessageTrade messageTrade)
        {
            //int qtdF = 0;
            //int totalTemp = 0;
            string retorno = "";
            string ipRequest = this.GetIpRequest();
            MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

            try
			{

				if (messageTrade.market.Equals("XBSP"))
				{
					if ((messageTrade.loteDefault > 0) && messageTrade.qtd % messageTrade.loteDefault != 0)
					{
						throw new Exception("A quantidade da oredem deve ser múltiplos do lote padrão.");
					}
				}


				//if ((messageTrade.loteDefault > 0) && messageTrade.qtd % messageTrade.loteDefault != 0)
				//{
				//    totalTemp = messageTrade.qtd;
				//    qtdF = messageTrade.qtd % messageTrade.loteDefault;
				//    totalTemp = totalTemp - qtdF;


				//    if (totalTemp > 0)
				//    {
				//        messageTrade.qtd = totalTemp;

				//        retorno += await _webFeederServiceInstance.SendNewOrderSingle(
				//                                                            messageTrade.account,
				//                                                            messageTrade.market,
				//                                                            messageTrade.price,
				//                                                            messageTrade.quote,
				//                                                            messageTrade.qtd,
				//                                                            messageTrade.validityorder,
				//                                                            messageTrade.targettrigger,
				//                                                            messageTrade.targetlimit,
				//                                                            messageTrade.stoptrigger,
				//                                                            messageTrade.stoplimit,
				//                                                            messageTrade.type,
				//                                                            messageTrade.side,
				//                                                            messageTrade.username,
				//                                                            messageTrade.orderstrategy,
				//                                                            messageTrade.loteDefault,
				//                                                            ipRequest,
				//                                                            messageTrade.timeinforce
				//                                                        );

				//    }

				//    if (qtdF < messageTrade.loteDefault)
				//    {
				//        messageTrade.quote = messageTrade.quote + 'F';
				//        messageTrade.qtd = qtdF;

				//        retorno += await _webFeederServiceInstance.SendNewOrderSingle(
				//                                                            messageTrade.account,
				//                                                            messageTrade.market,
				//                                                            messageTrade.price,
				//                                                            messageTrade.quote,
				//                                                            messageTrade.qtd,
				//                                                            messageTrade.validityorder,
				//                                                            messageTrade.targettrigger,
				//                                                            messageTrade.targetlimit,
				//                                                            messageTrade.stoptrigger,
				//                                                            messageTrade.stoplimit,
				//                                                            messageTrade.type,
				//                                                            messageTrade.side,
				//                                                            messageTrade.username,
				//                                                            messageTrade.orderstrategy,
				//                                                            messageTrade.loteDefault,
				//                                                            ipRequest,
				//                                                            messageTrade.timeinforce
				//                                                        );
				//    }
				//    else
				//    {
				//        messageTrade.qtd = totalTemp - qtdF;

				//        retorno += await _webFeederServiceInstance.SendNewOrderSingle(
				//                                                            messageTrade.account,
				//                                                            messageTrade.market,
				//                                                            messageTrade.price,
				//                                                            messageTrade.quote,
				//                                                            messageTrade.qtd,
				//                                                            messageTrade.validityorder,
				//                                                            messageTrade.targettrigger,
				//                                                            messageTrade.targetlimit,
				//                                                            messageTrade.stoptrigger,
				//                                                            messageTrade.stoplimit,
				//                                                            messageTrade.type,
				//                                                            messageTrade.side,
				//                                                            messageTrade.username,
				//                                                            messageTrade.orderstrategy,
				//                                                            messageTrade.loteDefault,
				//                                                            ipRequest,
				//                                                            messageTrade.timeinforce
				//                                                        );
				//    }
				//}
				//else
				//{
				//retorno = await _webFeederServiceInstance.SendNewOrderSingle(
				//                                                      messageTrade.account,
				//                                                      messageTrade.market,
				//                                                      messageTrade.price,
				//                                                      messageTrade.quote,
				//                                                      messageTrade.qtd,
				//                                                      messageTrade.validityorder,
				//                                                      messageTrade.targettrigger,
				//                                                      messageTrade.targetlimit,
				//                                                      messageTrade.stoptrigger,
				//                                                      messageTrade.stoplimit,
				//                                                      messageTrade.type,
				//                                                      messageTrade.side,
				//                                                      messageTrade.username,
				//                                                      messageTrade.orderstrategy,
				//                                                      messageTrade.loteDefault,
				//                                                      ipRequest,
				//                                                      messageTrade.timeinforce
				//                                                    );
				//}

				retorno = await _webFeederServiceInstance.SendNewOrderSingle(
																			messageTrade.account,
																			messageTrade.market,
																			messageTrade.price,
																			messageTrade.quote,
																			messageTrade.qtd,
																			messageTrade.validityorder,
																			messageTrade.targettrigger,
																			messageTrade.targetlimit,
																			messageTrade.stoptrigger,
																			messageTrade.stoplimit,
																			messageTrade.type,
																			messageTrade.side,
																			messageTrade.username,
																			messageTrade.orderstrategy,
																			messageTrade.loteDefault,
																			ipRequest,
																			messageTrade.timeinforce,
                                                                            tokenJWT
																			);

				return Ok(retorno);
			}
			catch (Exception ex)
			{
				return this.GetStatusCode(ex);
			}
		}

		#endregion

		#region SendEditOrder

        /// <summary>
        /// Edita uma ordem
        /// </summary>
        /// <param name="messageTrade"></param>
        /// <returns></returns>
        [HttpPost("SendEditOrder")]
        public async Task<IActionResult> SendEditOrder([FromBody]MessageTrade messageTrade)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.SendEditOrder(
																			 messageTrade.account,
																				messageTrade.origclordid,
																				messageTrade.orderid,
																				messageTrade.market,
																				messageTrade.price,
																				messageTrade.quote,
																				messageTrade.qtd,
																				messageTrade.maxfloor,
																				messageTrade.validityorder,
																				messageTrade.targettrigger,
																				messageTrade.targetlimit,
																				messageTrade.stoptrigger,
																				messageTrade.stoplimit,
																				messageTrade.movingstart,
																				messageTrade.initialchangetype,
																				messageTrade.type,
																				messageTrade.initialchange,
																				messageTrade.pricepercentage,
																				messageTrade.side,
																				messageTrade.username,
																				messageTrade.enteringtrader,
																				ipRequest,
																				messageTrade.timeinforce,
                                                                                tokenJWT));

			}
			catch (Exception ex)
			{
				return this.GetStatusCode(ex);
			}
		}

		#endregion

		#region CancelOrderByQuote (INATIVO)

        ///// <summary>
        ///// Cancela Todas as Ordens em Aberto do Ativo Informado 
        ///// </summary>
        ///// <param name="messageCancelOrder"></param>
        ///// <returns></returns>
        //[HttpPost("CancelOrderByQuote")]
        //public async Task<IActionResult> CancelOrderByQuote([FromBody]MessageCancelOrder messageCancelOrder)
        //{
        //    try
        //    {
        //        var retorno = "";
        //        string ipRequest = this.GetIpRequest();
        //        var messageHistoryOrder = new MessageHistoryOrderModel();

		//        messageHistoryOrder.account = messageCancelOrder.account;
		//        messageHistoryOrder.market = messageCancelOrder.market;
		//        messageHistoryOrder.symbol = messageCancelOrder.quote;
		//        //TODO:Alterar status para ABERTO quando liberar a atualizacao do WebFeeder
		//        //messageHistoryOrder.status = "Open";

		//        var orders = await _webFeederServiceInstance.GetHistoryOrder(
		//                                           messageHistoryOrder.account,
		//                                           messageHistoryOrder.market,
		//                                           messageHistoryOrder.dataInicio,
		//                                           messageHistoryOrder.dataFim,
		//                                           messageHistoryOrder.symbol,
		//                                           messageHistoryOrder.status,
		//                                           messageHistoryOrder.direction,
		//                                           messageHistoryOrder.orderID,
		//                                           messageHistoryOrder.type,
		//                                           messageHistoryOrder.statusGroup
		//                                           );

		//        var ordersList = JsonConvert.DeserializeObject<HistoryOrderModel>(orders);

		//        if (ordersList.listBeans.Count > 0)
		//        {
		//            foreach (ListBean ordem in ordersList.listBeans)
		//            {
		//                messageCancelOrder.account = ordem.account.value;
		//                messageCancelOrder.username = ordem.username.value;
		//                messageCancelOrder.orderid = ordem.orderID.value;
		//                messageCancelOrder.origclordid = ordem.clOrdID.value;
		//                messageCancelOrder.market = ordem.mercado.value;
		//                messageCancelOrder.quote = ordem.quote.value;
		//                messageCancelOrder.qtd = ordem.quantityRemaining.value;
		//                if (ordem.side.value.Equals("COMPRA") || ordem.side.value.Equals("Compra"))
		//                {
		//                    messageCancelOrder.side = "BUY";
		//                }
		//                else
		//                {
		//                    messageCancelOrder.side = "SELL";
		//                }

		//                if (retorno != "")
		//                {
		//                    retorno += ",";
		//                }

		//                retorno += await _webFeederServiceInstance.CancelOrder(
		//                                                    messageCancelOrder.username,
		//                                                    messageCancelOrder.orderid,
		//                                                    messageCancelOrder.origclordid,
		//                                                    messageCancelOrder.market,
		//                                                    messageCancelOrder.quote,
		//                                                    messageCancelOrder.qtd,
		//                                                    messageCancelOrder.side,
		//                                                    messageCancelOrder.enteringtrader,
		//                                                    messageCancelOrder.account,
		//                                                    ipRequest
		//                                                    );

		//            }


		//        }
		//        else
		//        {
		//            retorno = "Não foram encontrados ordens em aberto!";
		//        }

		//        return Ok("[" + retorno + "]");

		//    }
		//    catch (Exception ex)
		//    {
		//        return this.GetStatusCode(ex);
		//    }
		//}

		#endregion

		#region CancelOrder

        /// <summary>
        /// Cancela apenas uma ordem
        /// </summary>
        /// <param name="messageCancelOrder"></param>
        /// <returns></returns>
        [HttpPost("CancelOrder")]
        public async Task<IActionResult> CancelOrder([FromBody]MessageCancelOrder messageCancelOrder)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.CancelOrder(
																	messageCancelOrder.username,
																	messageCancelOrder.orderid,
																	messageCancelOrder.origclordid,
																	messageCancelOrder.market,
																	messageCancelOrder.quote,
																	messageCancelOrder.qtd,
																	messageCancelOrder.side,
																	messageCancelOrder.enteringtrader,
																	messageCancelOrder.account,
																	ipRequest,
                                                                    tokenJWT
																	));

			}
			catch (Exception ex)
			{
				return this.GetStatusCode(ex);
			}
		}

		#endregion

		#region CancelAllOrderHistory

        /// <summary>
        /// Cancela todas as ordens apartir da grid da tela de Historico
        /// </summary>
        /// <param name="ListHistoryOrder"></param>
        /// <returns></returns>
        [HttpPost("CancelAllOrderHistory")]
        public async Task<IActionResult> CancelAllOrderHistory([FromBody]List<HistoryOrderModel> ListHistoryOrder)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                var retorno = "";
                var messageCancelOrder = new MessageCancelOrder();

				if (ListHistoryOrder.Count > 0)
				{
					foreach (var ordem in ListHistoryOrder)
					{
						messageCancelOrder.account = ordem.account;
						messageCancelOrder.username = ordem.username;
						messageCancelOrder.orderid = ordem.orderID;
						messageCancelOrder.origclordid = ordem.clOrdID;
						messageCancelOrder.market = ordem.market;
						messageCancelOrder.quote = ordem.quote;
						messageCancelOrder.qtd = ordem.quantityRemaining;
						if (ordem.buyOrSell.Equals("C"))
						{
							messageCancelOrder.side = "BUY";
						}
						else
						{
							messageCancelOrder.side = "SELL";
						}

						if (retorno != "")
						{
							retorno += ",";
						}

						retorno += await _webFeederServiceInstance.CancelOrder(
																				messageCancelOrder.username,
																				messageCancelOrder.orderid,
																				messageCancelOrder.origclordid,
																				messageCancelOrder.market,
																				messageCancelOrder.quote,
																				messageCancelOrder.qtd,
																				messageCancelOrder.side,
																				messageCancelOrder.enteringtrader,
																				messageCancelOrder.account,
																				ipRequest,
                                                                                tokenJWT
																				);

					}

				}
				//else
				//{
				//    retorno = "Não foram encontrados ordens em aberto!";
				//}

				return Ok("[" + retorno + "]");

			}
			catch (Exception ex)
			{
				return this.GetStatusCode(ex);
			}
		}

		#endregion

		#region GetPositionBovespa

        /// <summary>
        /// Retorna dados da posição de Bovespa
        /// </summary>
        /// <param name="account">Número da conta (Obrigatório)</param>
        /// <param name="quote">Ativo (Opcional)</param>
        /// <returns></returns>
        [HttpGet("positionBovespa")]
        public async Task<IActionResult> GetPositionBovespa(string account, string quote)
        {
            try
            {
                if (string.IsNullOrEmpty(account))
                {
                    return BadRequest("O parâmetro account é obrigatório.");
                }

                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetPositionBovespa(account, quote, ipRequest, tokenJWT));

			}
			catch (Exception ex)
			{
				return this.GetStatusCode(ex);
			}
		}

		#endregion

		#region GetPositionBmf

        /// <summary>
        /// Retorna dados da posição de BMF
        /// </summary>
        /// <param name="account">Número da conta (Obrigatório)</param>
        /// <param name="quote">Ativo (Opcional)</param>
        /// <returns></returns>
        [HttpGet("positionBmf")]
        public async Task<IActionResult> GetPositionBmf(string account, string quote)
        {
            try
            {
                if (string.IsNullOrEmpty(account))
                {
                    return BadRequest("O parâmetro account é obrigatório.");
                }

                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetPositionBmf(account, quote, ipRequest, tokenJWT));

			}
			catch (Exception ex)
			{
				return this.GetStatusCode(ex);
			}
		}

		#endregion

		#region GetDayPosition

		/// <summary>
		/// Retorna dados da posição do dia
		/// </summary>
		/// <param name="account">Número da conta (Obrigatório)</param>
		/// <param name="quote">Ativo (Opcional)</param>
		/// <returns></returns>
		[HttpGet("dayPosition")]
		public async Task<IActionResult> GetDayPosition(string account, string quote)
		{
			try
			{
				if (string.IsNullOrEmpty(account))
				{
					return BadRequest("O parâmetro account é obrigatório.");
				}

                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetDayPosition(account, quote, ipRequest, tokenJWT));

			}
			catch (Exception ex)
			{
				return GetStatusCode(ex);
			}
		}

		#endregion

		#region GetFinancialAccountInformation

		/// <summary>
		/// Retorna dados da posição do dia
		/// </summary>
		/// <param name="account">Número da conta (Obrigatório)</param>
		/// <param name="market">Mercado (XBSP ou XBMF) </param>
		/// <returns></returns>
		[HttpGet("financialAccountInformation")]
		public async Task<IActionResult> GetFinancialAccountInformation(string account, string market)
		{
			try
			{
				if (string.IsNullOrEmpty(account))
				{
					return BadRequest("O parâmetro account é obrigatório.");
				}

				if (string.IsNullOrEmpty(account))
				{
					return BadRequest("O parâmetro market é obrigatório.");
				}

                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetFinancialAccountInformation(account, market, ipRequest, tokenJWT));

			}
			catch (Exception ex)
			{
				return GetStatusCode(ex);
			}
		}

		#endregion

		#region GetFinancialAccountInformationCompl

		/// <summary>
		/// Retorna dados da posição do dia
		/// </summary>
		/// <param name="account">Número da conta (Obrigatório)</param>
		/// <param name="market">Mercado (XBSP ou XBMF) </param>
		/// <returns></returns>
		[HttpGet("financialAccountInformationCompl")]
		public async Task<IActionResult> GetFinancialAccountInformationCompl(string account, string market)
		{
			try
			{
				if (string.IsNullOrEmpty(account))
				{
					return BadRequest("O parâmetro account é obrigatório.");
				}

				if (string.IsNullOrEmpty(account))
				{
					return BadRequest("O parâmetro market é obrigatório.");
				}

                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetFinancialAccountInformationCompl(account, market, ipRequest, tokenJWT));

			}
			catch (Exception ex)
			{
				return GetStatusCode(ex);
			}
		}

		#endregion

		#region GetLeverageQuote

		/// <summary>
		/// Retorna dados da alavancagem do ativo
		/// </summary>
		/// <param name="account">Número da conta (Obrigatório)</param>
		/// <param name="market">Mercado (XBSP ou XBMF) </param>
		/// <param name="quote">Ativo (Obrigatório)</param>
		/// <returns></returns>
		[HttpGet("leverageQuote")]
		public async Task<IActionResult> GetLeverageQuote(string account, string market, string quote)
		{
			try
			{
				if (string.IsNullOrEmpty(account))
				{
					return BadRequest("O parâmetro account é obrigatório.");
				}

				if (string.IsNullOrEmpty(market))
				{
					return BadRequest("O parâmetro market é obrigatório.");
				}

				if (string.IsNullOrEmpty(quote))
				{
					return BadRequest("O parâmetro quote é obrigatório.");
				}

                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetLeverageQuote(account, market, quote, ipRequest, tokenJWT));

			}
			catch (Exception ex)
			{
				return GetStatusCode(ex);
			}
		}

		#endregion

		#region GetStatementAccount'''

		[HttpGet("statementAccount")]
		public async Task<IActionResult> GetStatementAccount(string account, string market, string dateStart, string dateEnd)
		{
			try
			{
				if (string.IsNullOrEmpty(account))
				{
					return BadRequest("O parâmetro account é obrigatório.");
				}

				if (string.IsNullOrEmpty(account))
				{
					return BadRequest("O parâmetro market é obrigatório.");
				}

				if (string.IsNullOrEmpty(dateStart))
				{
					return BadRequest("O parâmetro data inicial é obrigatório.");
				}

				if (string.IsNullOrEmpty(dateEnd))
				{
					return BadRequest("O parâmetro data final é obrigatório.");
				}

                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetStatementAccount(account, market, dateStart, dateEnd, ipRequest, tokenJWT));

			}
			catch (Exception ex)
			{
				return GetStatusCode(ex);
			}
		}

		#endregion

		#region Garantias


		/// <summary>
		/// Mensagem de requisição para busca das garantias
		/// </summary>
		/// <param name="account">Número da conta (Obrigatório)</param>
		/// <returns></returns>
		[HttpGet("warrantyList")]
		public async Task<IActionResult> GetWarrantyList(string account)
		{
			try
			{
				if (string.IsNullOrEmpty(account))
				{
					return BadRequest("O parâmetro account é obrigatório.");
				}

                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetWarrantyList(account, ipRequest, tokenJWT));

			}
			catch (Exception ex)
			{
				return GetStatusCode(ex);
			}
		}

		/// <summary>
		/// Mensagem de requisição para busca das garantias
		/// </summary>
		/// <param name="account">Número da conta (Obrigatório)</param>
		/// <returns></returns>
		[HttpGet("warrantyOperation")]
		public async Task<IActionResult> GetWarrantyOperation(string account)
		{
			try
			{
				if (string.IsNullOrEmpty(account))
				{
					return BadRequest("O parâmetro account é obrigatório.");
				}

                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetWarrantyOperation(account, ipRequest, tokenJWT));

			}
			catch (Exception ex)
			{
				return GetStatusCode(ex);
			}
		}

		#endregion

		#region WarrantyOperationUpdate

        /// <summary>
        /// Realiza o envio de ordens via home broker
        /// </summary>
        /// <param name="messageTrade"></param>
        /// <returns></returns>
        [HttpPost("warrantyOperationUpdate")]
        public async Task<IActionResult> SendWarrantyOperationUpdate([FromBody]MessageTrade messageTrade)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.SendWarrantyOperationUpdate(
																					messageTrade.account,
																					messageTrade.warrantyID,
																					messageTrade.descWarranty,
																					messageTrade.quantity,
																					messageTrade.valueWarranty,
																					messageTrade.operation,
																					ipRequest,
                                                                                    tokenJWT
																				));

			}
			catch (Exception ex)
			{
				return this.GetStatusCode(ex);
			}
		}

		#endregion


		#region GetLeverageQuote

		/// <summary>
		/// Retorna dados da alavancagem do ativo
		/// </summary>
		/// <param name="account">Número da conta (Obrigatório)</param>
		/// <param name="market">Mercado (XBSP ou XBMF) </param>
		/// <param name="date">Ativo (Obrigatório)</param>
		/// <returns></returns>
		[AllowAnonymous]
		[HttpGet("brokeragePDF")]
		public async Task<IActionResult> GetBrokeragePDF(string account, string market, string date)
		{
			try
			{
				if (string.IsNullOrEmpty(account))
				{
					return BadRequest("O parâmetro account é obrigatório.");
				}

				if (string.IsNullOrEmpty(market))
				{
					return BadRequest("O parâmetro market é obrigatório.");
				}

				//if (string.IsNullOrEmpty(quote))
				//{
				//    return BadRequest("O parâmetro quote é obrigatório.");
				//}

                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                var stream = (await _webFeederServiceInstance.getBrokeragePDF(account, market, date, ipRequest, tokenJWT));

				return File(stream, "application/pdf", "brokeragePDF_" + account + ".pdf");


			}
			catch (Exception ex)
			{
				return GetStatusCode(ex);
			}
		}

		#endregion

		#region GetBrokerAccountList

		/// <summary>
		/// Retorna dados de account do usuário
		/// </summary>
		/// <param name="login">Login OMS</param>
		/// <returns></returns>
		[AllowAnonymous]
		[HttpGet("brokerAccountList")]
		public async Task<IActionResult> GetBrokerAccountList(string login)
		{
			try
			{
				if (string.IsNullOrEmpty(login))
				{
					return BadRequest("O parâmetro login é obrigatório.");
				}

				string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetBrokerAccountList(login, ipRequest, tokenJWT));


			}
			catch (Exception ex)
			{
				return GetStatusCode(ex);
			}
		}

		#endregion

	}
}

