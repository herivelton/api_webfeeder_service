﻿using System;
using System.Threading.Tasks;
using btg.homebroker.api.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;
//using Microsoft.Extensions.Logging;
using Serilog;

namespace btg.homebroker.api.Controllers
{

    [EnableCors("AllowCors"), Route("services/[controller]")]
    [Authorize]
    public class QuotesController : BaseController
    {
        #region Construtor

        //private readonly ILogger _logger;
        private IMemoryCache _cache;
        private static BLL.WebfeederServicesBLL _webFeederServiceInstance;

        public QuotesController(BLL.WebfeederServicesBLL webFeederServiceInstance, IMemoryCache memoryCache) //, ILogger logger)
        {
            _webFeederServiceInstance = webFeederServiceInstance;
            _webFeederServiceInstance._logger = Log.Logger;
            _cache = memoryCache;
            //_logger = logger;
        }

        #endregion

        /// <summary>
        /// Consultar Ativos
        /// </summary>
        /// <param name="query">descrição do ativo</param>
        /// <param name="qtdRegisters">quantidade de registros a serem retornados</param>
        /// <param name="getFractional">procura por ativos fracionados</param>
        /// <param name="getOptions">procura por opções</param>
        /// <returns>Lista dos ativos encontrados</returns>
        [HttpGet("quotesQuery")]
        public async Task<IActionResult> GetQuotesQuery(string query, int qtdRegisters, string getFractional, string getOptions)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                int sizeSearch = 2;
                string retorno = "";
                if (query.Length >= sizeSearch)
                {
                    string keysEntry = "GetQuotesQuery#" + query;
                    // Look for cache key.
                    if (!_cache.TryGetValue(keysEntry, out retorno))
                    {
                        retorno = await _webFeederServiceInstance.GetQuoteQuery(query, qtdRegisters, getFractional, getOptions, ipRequest, tokenJWT);

                        // Set cache options.
                        var cacheEntryOptions = new MemoryCacheEntryOptions()
                            // Keep in cache for this time, reset time if accessed.
                            .SetSlidingExpiration(TimeSpan.FromHours(10));

                        // Save data in cache.
                        _cache.Set(keysEntry, retorno, cacheEntryOptions);
                    }
                    else
                    {
                        this.LogMessageCache("services/quotes/quotesQuery?query=", query, ipRequest);
                    }
                }

                return Ok(retorno);
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// Consultar os ativos
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("quoteInformation")]
        public async Task<IActionResult> GetQuoteInformation(string query)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);
                //if (tokenJWT.ipRequest != tokenJWT.ipToken)
                //{
                //    this.LogMessageCache("services/quotes/quoteInformation?description=", tokenJWT.ipRequest, ipRequest);
                //}

                string retorno = "";
                //string keysEntry = "GetQuoteInformation#" + query;
                //// Look for cache key.
                //if (!cache.TryGetValue(keysEntry, out retorno))
                //{
                retorno = await _webFeederServiceInstance.GetQuoteInformation(query, ipRequest, tokenJWT);

                //    // Set cache options.
                //    var cacheEntryOptions = new MemoryCacheEntryOptions()
                //        // Keep in cache for this time, reset time if accessed.
                //        .SetSlidingExpiration(TimeSpan.FromHours(10));

                //    // Save data in cache.
                //    _cache.Set(keysEntry, retorno, cacheEntryOptions);
                //}
                //else
                //{
                //    this.LogMessageCache("services/quotes/quoteInformation?description=", query, ipRequest);
                //}

                return Ok(retorno);
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        //[HttpGet("listMarket")]
        //public string GetlistMarket()
        //{
        //    try
        //    {
        //        _logger.LogInformation("get/listMarket - {MachineName}!", machine);

        //        return _webFeederServiceInstance.GetListMarketString();
        //    }
        //    catch (Exception ex)
        //    {
        //        //return this.GetStatusCode(ex);
        //        return ex;
        //    }
        //}

        /// <summary>
        /// Consultar dados do ativo
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("quote")]
        public async Task<IActionResult> GetQuote(string query)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                //// string retorno = "";
                //string keysEntry = "GetQuote#" + query;

                //var cacheEntry = await
                //    _cache.GetOrCreateAsync(keysEntry, entry =>
                //    {
                //        entry.SlidingExpiration = TimeSpan.FromSeconds(5);
                //        return Task.FromResult(_webFeederServiceInstance.GetQuoteString(query));
                //    });

                //return Ok(cacheEntry);

                ////// Look for cache key.
                ////if (!_cache.TryGetValue(keysEntry, out retorno))
                ////{
                ////    retorno = await _webFeederServiceInstance.GetQuoteString(query);

                ////    // Set cache options.
                ////    var cacheEntryOptions = new MemoryCacheEntryOptions()
                ////        // Keep in cache for this time, reset time if accessed.
                ////        .SetSlidingExpiration(TimeSpan.FromSeconds(10));

                ////    // Save data in cache.
                ////    _cache.Set(keysEntry, retorno, cacheEntryOptions);
                ////}
                ////else
                ////{
                ////    this.LogMessageCache("services/quotes/quote/", query);
                ////}

                ////return Ok(retorno);

                return Ok(await _webFeederServiceInstance.GetQuoteString(query, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// Consultar os setores por mercado
        /// </summary>
        /// <param name="market">bovespa ou bmf</param>
        /// <returns></returns>
        [HttpGet("marketSectors")]
        public async Task<IActionResult> GetMarketSectors(string market)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                string retorno = "";
                if (!String.IsNullOrEmpty(market))
                {
                    string keysEntry = "GetMarketSectors#" + market;
                    // Look for cache key.
                    if (!_cache.TryGetValue(keysEntry, out retorno))
                    {
                        retorno = await _webFeederServiceInstance.GetMarketSectors(market, ipRequest, tokenJWT);

                        // Set cache options.
                        var cacheEntryOptions = new MemoryCacheEntryOptions()
                            // Keep in cache for this time, reset time if accessed.
                            .SetSlidingExpiration(TimeSpan.FromHours(10));

                        // Save data in cache.
                        _cache.Set(keysEntry, retorno, cacheEntryOptions);
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(market))
                    {
                        this.LogMessageCache("services/quotes/marketSectors?market=", market, ipRequest);
                    }
                }


                return Ok(retorno);
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// Consultar os ativos por subSetores
        /// </summary>
        /// <param name="sub">id subSetor</param>
        /// <returns></returns>
        [HttpGet("marketSubSectorQuotes")]
        public async Task<IActionResult> GetMarketSubSectorQuotes(string sub)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                string retorno = "";
                if (!String.IsNullOrEmpty(sub))
                {
                    string keysEntry = "GetMarketSubSectorQuotes#" + sub;
                    // Look for cache key.
                    if (!_cache.TryGetValue(keysEntry, out retorno))
                    {
                        retorno = await _webFeederServiceInstance.GetMarketSubSectorQuotes(sub, ipRequest, tokenJWT);

                        // Set cache options.
                        var cacheEntryOptions = new MemoryCacheEntryOptions()
                            // Keep in cache for this time, reset time if accessed.
                            .SetSlidingExpiration(TimeSpan.FromHours(10));

                        // Save data in cache.
                        _cache.Set(keysEntry, retorno, cacheEntryOptions);
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(sub))
                    {
                        this.LogMessageCache("services/quotes/marketSubSectorQuotes?sub=", sub, ipRequest);
                    }
                }


                return Ok(retorno);
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// Lista de maiores baixas
        /// </summary>
        /// <param name="indice"></param>
        /// <returns></returns>
        [HttpGet("fallList")]
        public async Task<IActionResult> GetfallList(string indice)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetFallList(indice, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// Lista maiores altas
        /// </summary>
        /// <param name="indice"></param>
        /// <returns></returns>
        [HttpGet("highList")]
        public async Task<IActionResult> GethighList(string indice)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetHighList(indice, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// Lista de maiores volumes
        /// </summary>
        /// <param name="indice"></param>
        /// <returns></returns>
        [HttpGet("highVolumeList")]
        public async Task<IActionResult> GethighVolumeList(string indice)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetHighVolumeList(indice, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// Lista de menores volumes
        /// </summary>
        /// <param name="indice"></param>
        /// <returns></returns>
        [HttpGet("fallVolumeList")]
        public async Task<IActionResult> GetFallVolumeList(string indice)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetFallVolumeList(indice, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// Consultar lista de simbolos
        /// </summary>
        /// <param name="market"></param>
        /// <param name="typeQuote"></param>
        /// <param name="description"></param>
        /// <param name="speak"></param>
        /// <param name="expiration"></param>
        /// <param name="company"></param>
        /// <param name="contract"></param>
        /// <returns></returns>
        [HttpGet("listQuote")]
        public async Task<IActionResult> GetListQuote(string market, string typeQuote, string description, string speak, string expiration, string company, string contract)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetListQuote(market, typeQuote, description, speak, expiration, company, contract, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        [HttpGet("quoteExpression")]
        public async Task<IActionResult> GetQuoteExpression(string speak)
        {
            try
            {
                string market = "2";
                string typeQuote = "-1";
                string description = "-";
                string expiration = "1";
                string company = "-";
                string contract = "-";
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetListQuote(market, typeQuote, description, speak, expiration, company, contract, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        [HttpGet("listTypeQuotes")]
        public async Task<IActionResult> GetListTypeQuotes()
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetListTypeQuotes(ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        [HttpGet("playerNames")]
        public async Task<IActionResult> GetPlayerNames()
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                string retorno = "";
                string keysEntry = "GetPlayerNames#Key";
                // Look for cache key.
                if (!_cache.TryGetValue(keysEntry, out retorno))
                {
                    retorno = await _webFeederServiceInstance.GetPlayerNames("", ipRequest, tokenJWT);

                    // Set cache options.
                    var cacheEntryOptions = new MemoryCacheEntryOptions()
                        // Keep in cache for this time, reset time if accessed.
                        .SetSlidingExpiration(TimeSpan.FromHours(10));

                    // Save data in cache.
                    _cache.Set(keysEntry, retorno, cacheEntryOptions);
                }
                else
                {
                    this.LogMessageCache("services/quotes/playerNames", "", ipRequest);
                }
                return Ok(retorno);

            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        [HttpGet("listContractBMF")]
        public async Task<IActionResult> GetListContractBMF()
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetListContractBMF(ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        [HttpGet("listCompanyMarket")]
        public async Task<IActionResult> GetListCompanyMarket(string marketCode, string name)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetListCompanyMarket(marketCode, name, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// Lista dos mais negociadas a vista
        /// </summary>
        /// <param name="market"></param>
        /// <returns></returns>
        [HttpGet("highListNegotiation")]
        public async Task<IActionResult> GetHighListNegotiation(string market)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetHighListNegotiation(market, ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }


        /// <summary>
        /// Retorna índices
        /// </summary>
        /// <returns></returns>
        [HttpGet("finantialRatios")]
        public async Task<IActionResult> GetFinantialRatios()
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetFinantialRatios(ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// Retorna informações câmbio
        /// </summary>
        /// <returns></returns>
        [HttpGet("exchange")]
        public async Task<IActionResult> GetExchange()
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetExchange(ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

        /// <summary>
        /// Retorna informações commodities
        /// </summary>
        /// <returns></returns>
        [HttpGet("commodities")]
        public async Task<IActionResult> GetCommodities()
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetCommodities(ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }

    }
}
