﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using btg.homebroker.api.Model;
using Microsoft.AspNetCore.Authorization;
using Serilog;
using System;

namespace btg.homebroker.api.Controllers
{
#if DEBUG || LEROSA
    /// <summary>
    /// Consulta ofertas Tesouro Direto.
    /// </summary>
    /// <returns></returns>
    [EnableCors("AllowCors"), Route("services/[controller]")]
    [Authorize]
    public class FundsController : BaseController
    {

        #region Construtor

        //private readonly ILogger _logger;
        private static BLL.WebfeederServicesBLL _webFeederServiceInstance;

        public FundsController(BLL.WebfeederServicesBLL webFeederServiceInstance) //, ILogger logger)
        {
            _webFeederServiceInstance = webFeederServiceInstance;
            _webFeederServiceInstance._logger = Log.Logger;
        }

        #endregion

        /// <summary>
        /// Consulta ofertas Tesouro Direto.
        /// </summary>
        /// <returns></returns>
        [HttpGet("getFundsOffers")]
        public async Task<IActionResult> GetFundsOffers(string query)
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetFundsOffers(query, ipRequest));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);

            }
        }

    }
#endif
}
