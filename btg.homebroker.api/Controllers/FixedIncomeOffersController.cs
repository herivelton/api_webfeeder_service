﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using btg.homebroker.api.Model;
using Microsoft.AspNetCore.Authorization;
using Serilog;
using System;

namespace btg.homebroker.api.Controllers
{
 #if DEBUG || LEROSA
    [EnableCors("AllowCors"), Route("services/[controller]")]
    //[Authorize]
    public class FixedIncomeOffersController : BaseController
    {

    #region Construtor

        //private readonly ILogger _logger;
        private static BLL.WebfeederServicesBLL _webFeederServiceInstance;

        public FixedIncomeOffersController(BLL.WebfeederServicesBLL webFeederServiceInstance) //, ILogger logger)
        {
            _webFeederServiceInstance = webFeederServiceInstance;
            _webFeederServiceInstance._logger = Log.Logger;
            //_logger = logger;
        }

    #endregion

        /// <summary>
        /// Consulta ofertas renda fixa.
        /// </summary>
        /// <returns></returns>
        [HttpGet("getFixedIncomeOffers")]
        public async Task<IActionResult> GetFixedIncomeOffers()
        {
            try
            {
                string ipRequest = this.GetIpRequest();
                MessageTokenJWT tokenJWT = this.GetTokenJWT(ipRequest);

                return Ok(await _webFeederServiceInstance.GetFixedIncomeOffers(ipRequest, tokenJWT));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }


        /// <summary>
        /// Registrar uma aplicação do cliente no pre-boleto.
        /// </summary>
        /// <returns></returns>
        [HttpPost("applyFixedIncomeOffers")]
        public async Task<IActionResult> ApplyFixedIncomeOffers(MessageFixedIncomeModel messageFixedIncome)
        {
            try
            {
                string ipRequest = this.GetIpRequest();

                return Ok(await _webFeederServiceInstance.ApplyFixedIncomeOffers(messageFixedIncome, ipRequest));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }


        /// <summary>
        /// Registrar um pedido de resgate do cliente no pre-boleto.
        /// </summary>
        /// <returns></returns>
        [HttpPost("registerRescuePreBilletOrder")]
        public async Task<IActionResult> RegisterRescuePreBilletOrder(MessageFixedIncomeModel messageFixedIncome)
        {
            try
            {
                string ipRequest = this.GetIpRequest();

                return Ok(await _webFeederServiceInstance.RegisterRescuePreBilletOrder(messageFixedIncome, ipRequest));
            }
            catch (Exception ex)
            {
                return this.GetStatusCode(ex);
            }
        }
    }
#endif
}
