﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using System.Collections;

namespace btg.homebroker.api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Porta da API - 8080
            var url = "http://*:8080";
            var contentRoot = Directory.GetCurrentDirectory();

            if (args.Length > 0)
            {
                url = args[0];
            }

            Console.WriteLine($"URL: {url}");
            Console.WriteLine($"ContentRoot: {contentRoot}");

            //foreach (DictionaryEntry item in Environment.GetEnvironmentVariables())
            //{
            //    Console.WriteLine($"{item.Key}: {item.Value}");
            //}

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseUrls(url)
                //.UseContentRoot(Directory.GetCurrentDirectory())
                .UseContentRoot(contentRoot)
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
