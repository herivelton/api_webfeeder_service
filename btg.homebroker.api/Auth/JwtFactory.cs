﻿//using System;
//using System.Collections.Generic;
//using System.IdentityModel.Tokens.Jwt;
//using System.Security.Claims;
//using System.Security.Principal;
//using System.Threading.Tasks;
//using btg.homebroker.api.Model.Auth;
//using Microsoft.Extensions.Options;

//namespace btg.homebroker.api.Auth
//{
//    public class JwtFactory : IJwtFactory
//    {
//        private readonly JwtIssuerOptions _jwtOptions;

//        public JwtFactory(IOptions<JwtIssuerOptions> jwtOptions)
//        {
//            _jwtOptions = jwtOptions.Value;
//            ThrowIfInvalidOptions(_jwtOptions);
//        }

//        public async Task<string> GenerateEncodedToken(string userName, ClaimsIdentity identity)
//        {
//            var claims = new[]
//         {
//                 new Claim(JwtRegisteredClaimNames.Sub, userName),
//                 new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
//                 new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
//                 //identity.FindFirst(Helpers.Constants.Strings.JwtClaimIdentifiers.Rol),
//                 //identity.FindFirst(Helpers.Constants.Strings.JwtClaimIdentifiers.Id)
//             };

//            // Create the JWT security token and encode it.
//            var jwt = new JwtSecurityToken(
//                issuer: _jwtOptions.Issuer,
//                audience: _jwtOptions.Audience,
//                claims: claims,
//                notBefore: _jwtOptions.NotBefore,
//                expires: _jwtOptions.Expiration,
//                signingCredentials: _jwtOptions.SigningCredentials);

//            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

//            return encodedJwt;
//        }

//        public ClaimsIdentity GenerateClaimsIdentity(string userName, string id, string fullName, string email, string role, string ipRequest)
//        {
//            List<Claim> claims = new List<Claim>();

//            claims.Add(new Claim("Id", id));
//            claims.Add(new Claim("Guid", fullName));
//            claims.Add(new Claim("EMail", email));
//            claims.Add(new Claim("IP", ipRequest));


//            //if (string.IsNullOrEmpty(role))
//            //{
//            //  claims.Add(new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.Rol, Helpers.Constants.Strings.JwtClaims.Client));
//            //}
//            //else
//            //{
//            //  if (role.ToLower() == Constants.Roles.ANALYST.ToLower())
//            //  {
//            //    claims.Add(new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.Rol, Helpers.Constants.Strings.JwtClaims.Analyst));
//            //  }
//            //  else if (role.ToLower() == Constants.Roles.ADMINISTRATOR.ToLower())
//            //  {
//            //    claims.Add(new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.Rol, Helpers.Constants.Strings.JwtClaims.Administrator));
//            //  }
//            //}

//            return new ClaimsIdentity(new GenericIdentity(userName, "Token"), claims);
//        }

//        /// <returns>Date converted to seconds since Unix epoch (Jan 1, 1970, midnight UTC).</returns>
//        private static long ToUnixEpochDate(DateTime date)
//          => (long)Math.Round((date.ToUniversalTime() -
//                               new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero))
//                              .TotalSeconds);

//        private static void ThrowIfInvalidOptions(JwtIssuerOptions options)
//        {
//            if (options == null) throw new ArgumentNullException(nameof(options));

//            if (options.ValidFor <= TimeSpan.Zero)
//            {
//                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(JwtIssuerOptions.ValidFor));
//            }

//            if (options.SigningCredentials == null)
//            {
//                throw new ArgumentNullException(nameof(JwtIssuerOptions.SigningCredentials));
//            }

//            if (options.JtiGenerator == null)
//            {
//                throw new ArgumentNullException(nameof(JwtIssuerOptions.JtiGenerator));
//            }
//        }
//    }
//}